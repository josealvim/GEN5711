\documentclass{article}

\usepackage[portuguese]{babel}
\usepackage{hyperref}

\begin{document}
	\title{
		Resumo: \\
		\textbf{Contrato Didático}\\
		{\small Benedito Antonio da Silva}\\
		em 
		\textbf{“Educação Matemática uma Introdução”} (1999)
	} 
	
	\author{
		José Goudet Alvim\\
		\textnumero{}USP 9793968\\
		\texttt{jose.alvim@usp.br}
	}

	\maketitle

	O texto trata do conceito de “Contrato Didático” que o autor 
	defende ser essencialmente um modo social que rege a relação entre 
	docentes e discentes que se estabelece de maneira predominantemente
	tácita e que devem ser da ciência de uma pessoa exercendo função de 
	educadora devido às suas consequências pedagógicas. As questões 
	centrais abordadas são: 
	\begin{enumerate}
		\item	os efeitos colaterais que podem advir dos contratos 
			didáticos;
		\item	a origem e mecanismos por trás destes tais efeitos 
			deletérios;
		\item	a quebra contratual como dispositivo pedagógico;
	\end{enumerate}

	Neste resumo ficará disposto brevemente o entendimento do autor do 
	conceito de contrato didático, bem como os pontos agora citados que 
	são centrais para o texto; tudo de maneira sintética quando de todo
	possível. Neste resumo, colocarei comentários e elaborações que não 
	necessariamente correspondem às ideias do autor autor entre 
	colchetes: [esta é uma forma de não implicar que o dito foi afirmado 
	ou implicado por B. A. da Silva].

	O resumo está estruturado em três seções, a primeira é do contrato 
	didático como um conceito; a segunda, dos efeitos negativos que o 
	autor cita como advindo de problemas contratuais; terceiramente, 
	um resumo de como e porquê estes problemas surgem da relação entre 
	um professor ou professora e sua turma, contexto histórico e social 
	e outros fatores que constituem o chamado contrato didático.

	\section{Do Contrato Didático}
	Como dito no primeiro parágrafo, o contrato social é um modo regente
	de uma certa relação que é a determinante no ensino: a de aluno e 
	professor. O autor defende que há uma variedade de tais contratos em 
	existência, a variar com escolhas pedagógicas, avaliativas, e do 
	próprio conteúdo abordado; e este contrato tem valor regimental na 
	relação entre as partes citadas porque a relação entre elas está 
	estabelecida em uma instituição social que detém a responsabilidade 
	de transmitir o conhecimento: a escola.

	A escola como instituição social tem uma carga histórica, cultural e
	social que legitimam e estabelecem regras e códigos de conduta para 
	as pessoas que a frequentam, sejam alunos, alunas, professoras ou 
	professores. As expectativas, papeis e funções de cada parte e de 
	cada parte para com a outra não costumam ser explicitamente 
	estabelecidas [coisa comum em relações in\emph{formais}] e o autor 
	sustenta que uma quantidade de efeitos indesejáveis surgem da 
	incompreensão do contrato, ou de faltas das partes para com ele.

	O contrato didático no contexto da relação de uma professora e sua 
	turma -- por exemplo --, portanto é resultado das imagens recíprocas
	dela de sua turma, e da sua turma dela; das suas escolhas didáticas; 
	tema abordado; metodologia avaliativa; contexto histórico e um 
	espectro de ações atitudes mais sutís e indeterminadas, que 
	estabelece a natureza daquela relação professora-estudante.
	
	Algumas cláusulas viciosas de um contrato didático são citadas pelo 
	autor como tendo sido identificadas por Chevallard:
	\begin{enumerate}
		\item	\label{clause:answer questions}
			Toda pergunta feita a mim deve ser respondida,
			usando-se os dados do enunciado;
	
		\item	\label{clause:math is bullshit}
			Exercícios de matemática são fundalmentamente 
			desconexos da vida cotidiana, e qualquer semelhança é 
			pura coincidência; 

		\item	\label{clause:dumb parser}
			Certas palavras-chave estão correlacionadas com certas
			operações numéricas;
		

		\item	\label{clause:answers are computations}
			Minhas respostas são resoluções e computações, para as
			quais será atribuída uma correção (e é do meu interesse
			que esta seja positiva).
	\end{enumerate}

	\section{Dos Efeitos Adversos do Contrato}
	Centralmente, a tese defendida é que há certos tipos viciosos que 
	podem se fazer manifestos na relação de ensino. Entre estes, 
	identifique:
	\begin{enumerate}
			
		\item	\label{vice:alienation}
			Alienação das capacidades racionais de um estudante
			em pról da “resolução” de exercícios. O contrato 
			determina e demanda que qualquer questão tenha uma 
			solução, e que a solução advém de manipulações 
			arbitrárias [já aí se manifesta uma falha de 
			aprendizado] dos dados do enunciado, que são números
			largamente desprovidos de significado real [aí se 
			manifesta a alienação da faculdade racional] e 
			portanto está justificado “fazer contas” sem sentido
			para se obter uma resposta: pois de certa forma 
			todas outras contas também carecem sentido.

		\item	O chamado Efeito Topázio, onde -- por exemplo -- uma 
			estudante não tem a oportunidade de errar ou completar
			uma atividade onde ela encontrou dificuldade pois a 
			atitude de sua professora é a de “ajudar” e simplificar
			o problema até que ele tenha sido solucionado não pela 
			aluna mas essencialmente pela professora.

		\item	O abuso de comparações e simplificações de um tema 
			discutido [que, interessantemente, não precisam 
			diminuir a generalidade do problema; mas não basta uma
			conexão lógica formalmente forte, se a analogia não é 
			óbvia para o estudante (que já podia não estar 
			entendendo) a mudança deste problema para um outro não 
			é de serventia alguma].

		\item	A atribuição de compreensão própria de um conceito 
			devido a comportamentos absolutamente naturais e de 
			natureza não culta: abundam comportamentos que podem 
			acontecer de maneira deliberada aplicando-se conceitos 
			ou de maneira inata e intuitiva, mas o simples fato de 
			alguém ser capaz de fazê-lo não é indicativo de que o 
			conceito lhe seja familiar. [Afinal, as pombas voam há
			muito mais tempo que nós, não por isso sabem o 
			conhecimento “necessário” para o vôo].

		\item[[4.5.\hspace*{-0.5em}]]
			[Algo que não foi discutido pelo autor foi o “abuso” 
			consciente do contrato didático por parte de alunos,
			esta é uma adição que estou deixando aqui porque para 
			mim é interessante:
			é uma estratégia que já advoguei para meus colegas que 
			é justamente \emph{não buscar esclarecimentos}: 
			“se um prazo não foi estabelecido, se uma restrição não 
			foi determinada, não pergunte: pois se a resposta for 
			‘não’ você perde uma alternativa e se a resposta for 
			sim você não precisava ouvi-la”. 

			A expectativa é de que, como uma falta de clareza da 
			outra parte pode ter me induzido ao erro, e como o 
			trabalho prometido foi executado com a premissa, o 
			professor em questão não vai arbirar em meu desfavor.]

		\item	\label{vice:criteria changed}
			A mudança de critérios e objeto de estudos que ocorre 
			como uma renegociação tácita quando um novo tema ou 
			etapa pedagógica se inicia. A mudança de objeto ou 
			critério avaliativo desnorteia mesmo quando é explícita
			pois criou-se um costume internalizado. [Isto é 
			presente até no ensino superior, e mesmo quando tentam 
			dizer os critérios explicitamente: em 
			alguns cursos espera-se que um aluno “faça todas as 
			passagens”, mas certamente uma professora que pede isso 
			não espera nem deseja que uma aluna eventualmente lhe 
			entregue um \emph{Principia Mathematica} onde cada 
			aplicação da regra do corte ou 	Modus Ponens esteja 
			explicitada].
			
			[Por exemplo, pode-se esperar de uma aluna de graduação 
			em matemática pura um grau relativamente baixo de 
			formalismo no início da graduação, esta exigência 
			aumenta conforme corre o curso e ela pode inclusive 
			diminuir na pós-graduação; pois há uma confiança e 
			portanto um acordo que ambas as partes sabem completar 
			as lacunas de umas prova e “o que importa é a ideia 
			geral” e coisas deste tipo. 
			
			Isso, claro, tem motivo prático: seria impraticável 
			cobrir um tema como Topologia Algébrica com 
			\emph{precisão perfeita} -- matematicamente falando -- 
			e poderia até ser pedagogicamente deletério fazê-lo; 
			mas a compreensão de que há uma medida “aceitável” de 
			erro ou imprecisão é algo de que todo estudante 
			\emph{pode} vir a fazer uso: “acochambramento”]. 
	\end{enumerate}	

	\section{Da Origem destes Fenômenos Viciosos}
	
	A tese defendida pelo autor e justificada por Chevallard é que a 
	internalização de certas cláusulas viciosas 
	(\ref{clause:answer questions}, \ref{clause:math is bullshit} e 
	\ref{clause:dumb parser} entre outras) levam logicamente à suspensão 
	da faculdade racional de um aluno que se sente justificado e compelido
	a manipular os dados do enunciado de maneira arbitrária para que se 
	chegue a um número qualquer que deve ser a resposta da questão. 
	Que é característico do efeito perverso \ref{vice:alienation}. 

	O autor afirma que isto não se deve a uma ingenuidade devida à idade ou
	a uma deficiência inata de um ou outro estudante, mas sim de uma adesão
	incondicional às cláusulas tácitas de um contrato didático que este ou 
	esta estudante estavam acostumandos. [Portanto, na atividade 
	“performática” do estudante, este pode ser estimulado a um certo nível 
	“dadaísmo simbólico” na resolução de exercícios, que não é o objeto de 
	aprendizado].

	Um outro efeito negativo citado é o uso e ensino de algorítmos, regras 
	e técnicas para resolver problemas, tomando prioridade sobre os 
	problemas e o conhecimento que deveria ser o real objetivo de 
	transmissão. Isso deve ocorrer quando fica configurado que é mais 
	importante resolver exercícios do que saber de fato algum conteúdo que 
	os compreende.

	A teoria central da origem destes problemas, na opinião do autor, é a 
	da incompreensão ou mal-entendimento das minúcias e cláusulas diversas
	do contrato didático por uma de suas partes. O professor pode não 
	compreender que a sua atitude acaba por estabelecer um contrato com 
	cláusulas viciosas, ou o aluno pode não entender que sua dificuldade 
	advém de um desencontro da imagem do mesmo sobre a relação com o 
	conhecimento da aula, o professor, ou a atividade de estudo.
	\begin{quote}
		Grande parte das dificuldades dos alunos é causada pelos 
		efeitos	do contrato didático mal-colocado ou mal-entendido. 
		Este traz no seu bojo a marca da expectativa do professor em 
		relação à classe ou mesmo a um aluno em particular.	
	\end{quote}

	[Algo que me tomou um tempo foi entender porque o problema 
	\ref{vice:alienation} se deve a um mal-entendido do contrato didático:
	Este problema não é caracteristicamente resultante de um mal-entendido
	da parte dos alunos; eles estão sendo avaliados e efetivamente 
	treinados para agir daquela maneira. Não, o mal-entendido do contrato é
	da parte do professor ou professora que não percebeu que foram
	estabelecidas as cláusulas viciosas].

	Os outros efeitos negativos então também se dão por meio de 
	mal-entendidos: o efeito Topázio pode ser causado pela percepção que o 
	erro deve ser evitado pelo professor, e na tentativa de induzir o 
	acerto o exercício é invariavelmente solucionado mais pelo professor do
	que pelo próprio aluno: O carater social do trabalho de uma professora 
	ou professor, na caracterização de obrigação social o ensino “de tudo 
	que é necessário sobre o saber” leva a uma situação desconfortável, 
	onde pela necessidade de \emph{fazer} um aluno ou aluna a acertar, é 
	solapado o processo pedagógico em favor de um resultado que não
	reflete o sucesso do aprendizado.

	A boa disposição e tolerância de um professor pode levá-lo a 
	comprometer o conhecimento através de analogias infelizes, ou ainda 
	exigir menos do que o necessário dos seus alunos. Pois há, na relação 
	entre alunos e um professor, duas expectativas: a do professor que 
	imagina as capacidades dos alunos; e de um aluno que imagina o que o 
	professor pensa sobre a turma e ele mesmo 
	[na verdade há também a própria imagem do aluno de si, que também
	pode levar a problemas, mas o autor não fala dela]. 
	Estas expectativas limitam um docente a fazer perguntas de acordo com 
	a impressão dele do nível da sala; e a imagem que um professor tem de 
	sua turma também serve de critério de suficiência para a turma[: “para 
	quê fazer mais do que se espera de mim?” é a pergunta que caracteriza 
	este modo negativo].
	
	A falta de clareza na renegociação de contratos, que acontece natural
	e necessariamente no decorrer do processo de aprendizado, também induz 
	problemas quando as mudanças afetam o hábitos cognitivos e hipóteses 
	tácitas das partes envolvidas; o autor coloca isso no estudo de 
	geometria plana, que se inicia com formas concretas e migra para o 
	contexto abstrato que é regido por um outro conjunto de \emph{axiomas}.
	
	As conclusões finais então são a da constatação de que o contrato 
	didático é uma construção “provisória” e que o objetivo do aprendizado 
	é desfazer-se dela. 
	
	[Aqui convém fazer uma indicação, não é abordada uma objeção comum: o 
	“mas para que serve isso?” que indica a expectativa dos alunos de  
	um compromisso do professor e do conhecimento prometido para com a 
	utilidade imediata e corriqueira de um conhecimento culto, coisa 
	que certamente pode ocasionar um alienamento do interesse de uma 
	parcela de estudantes que não vêem aplicação de um conhecimento que é 
	abstrato \emph{par excellence}].
\end{document}
