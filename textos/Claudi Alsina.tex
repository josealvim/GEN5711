\documentclass{article}

\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{amsmath, amsfonts}
\usepackage[portuguese]{babel}
\usepackage{hyperref}
\usepackage{pgf,tikz}
\usetikzlibrary{arrows}

\definecolor{uququq}{rgb}{0.25,0.25,0.25}
\definecolor{zzttqq}{rgb}{0.6,0.2,0}
\definecolor{qqqqff}{rgb}{0,0,1}

\newcommand\ellipsis{[\ldots]}

\begin{document}
	\title{
		Resumo: \\
		\textbf{
			Why the professor must be a stimulating teacher
		}\\
		de\\
		\textbf{
			Claudi Alsina
		}
	} 
	
	\author{
		José Goudet Alvim\\
		\textnumero{}USP 9793968\\
		\texttt{jose.alvim@usp.br}
	}

	\maketitle

	Quando Claudi critica o que ele chama de mito de 
	pesquisadores-são-bons-professores: 
	“\textit{This university myth says that ‘researchers are ipso facto 
	good teachers… therefore the key criteria for selection and 
	promotion must be high quality research’.}” ele está correto na medida
	que de fato a habilidade didática de uma pessoa e sua produção 
	científica são propriedades largamente incorrelatas. Mas questão que 
	fica para mim é a seguinte (é uma questão multifacetada): em que medida
	podemos nos enfocar nas habilidades didáticas no corpo docente de uma 
	universidade?

	Pergunto isso porque o autor afirma que 
	“\textit{The ‘either-research-or-teaching’ polarity is false.}”, mas eu
	não tenho certeza. Não porque eu não tenha certeza de que não é 
	possível pesquisar \emph{o} ensino, ou aplicar a pesquisa de matemática
	\emph{ao} ensino. Mas a matemática a nível de graduação, na maneira que 
	os cursos estão estruturados, não representa a pesquisa em matemática, 
	e favorecer os “bons professores” desestimula \emph{sim} a prática de 
	matemática que não é “ensinável” em cursos de graduação.

	Não estamos vivendo em uma sociedade onde abundam recursos em nossos 
	departamentos: não faz um ano que a universidade de Leicester pretendia 
	-- não sei se chegou aos termos de fazê-lo, mas a intenção foi feita 
	manifesta -- demitir todo o seu corpo docente na área de matemática 
	pura como pesquisadores e recontratar um percentual exclusivamente 
	focado em ensino para suprir a necessidade instrumental de outras áreas
	mais atraentes.

	\begin{quote}
		The Vice-Chancellor of the University of Leicester, 
		Nishan Canagarajah, wants to lay off all 8 of their pure 
		mathematicians, including the only 2 women with permanent 
		positions, and then rehire just 3 of these mathematicians — 
		to do teaching, but not research. He and his flunkies claim:
		[…] to ensure a future research identity in AI, computational 
		modelling, digitalisation and data science requires ceasing 
		research in Pure Mathematics […]
		\marginpar{\cite{Baez21}}
	\end{quote}

	Desta forma, acho que se construírmos uma cultura na qual a qualidade 
	de aulas de matemática de nível de graduação tenha um valor muito maior
	veremos isto sendo priorizado \emph{sobre} as áreas menos “aplicáveis”
	a este fim: pesquisa fim e especializada. É importante notar que a 
	função de um professor universitário difere bastante da de um professor
	do ensino fundamental e médio, é -- a meu ver -- muito mais proveitoso 
	ter um professor inegavelmente ruim em sala mas um excelente 
	pesquisador: depois da aula você tem acesso a uma mente que é expoente 
	em uma área onde há pesquisa de ponta e através da qual você pode se 
	colocar no campo. Um exemplo concreto: há quem diga\footnote{
		aqui vai ficar citado como “corresponência pessoal” não 
		prejudicar quem falou.
	} para que o Peter T. Johnstone da universidade de Cambridge 
	tem aulas terríveis, mas não imagino que uma sala dele fique vazia.

	Meu argumento para o valor excepcional de “professores como 
	pesquisadores primeiro” depende -- pelo menos parcialmente -- de uma 
	coisa que Claudi afirma ser falsa: a disponibilidade e facilidade de 
	interação com estes professores.
	\begin{quote}
		Outside the classroom or the scheduled office hours there is 
		no place for further human interaction. The university walls 
		keep human nature out.
	\end{quote}
	e isto pode ser verdadeiro ou pode ser falso -- a depender do 
	departamento, da facilidade social ou “falta de vergonha” de um dado 
	aluno. Eu acredito que as universidades devem -- e se beneficiam de -- 
	fomentar um espírito de pesquisa, que é o que aproxima -- pois pesquisa
	não se faz sozinho -- os discentes dos docentes. Isto motiva um aluno 
	pois seu professor vai lhe dizer “faça tal disciplina”, “vamos fazer 
	uma iniciação científica”, “leia tal livro”, “vamos abrir esta conta”,
	“isto aí não ficou bom”.

	Isso é muito por conta do meu enorme privilégio de ter tido como 
	professor, desde o segundo semestre, o Hugo Luiz Mariano aqui do 
	departamento de Matemática. Cuja disponibilidade e generosidade com o 
	seu tempo e conhecimento é lendária. Mas abundam exemplos de 
	\emph{pessoas}-professoras simpáticas que, apesar de suas dificuldades 
	explicativas, pedagógicas e didáticas diversas, são bem sucedidos em 
	\emph{ensinar} e sobretudo -- talvez mais importante -- transformar o 
	aluno em pesquisador.

	Aqui fica claro que os dois propósitos de um professore de matemática 
	levam a duas interpretações. Para uma pessoa em um curso que use da 
	matemática apenas instrumentalmente e que não tenha interesse em se 
	aprofundar no tema, é realmente desnecessário que uma professora seja 
	dedicada à pesquisa; e consequentemente é mais desejável que ela seja 
	dedicada ao ensino. 

	Mas em uma instituição \emph{de pesquisa} e de formação de 
	pesquisadores, onde se espera que um aluno seja forçado a desenvolver 
	um nível de autonomia de estudo, a importância passa a ser colocada nas 
	outras coisas que a professora a pode oferecer: extensão e pesquisa.

	Agora que o grosso das minhas desavenças foram já exploradas, vamos aos
	pontos em comum. Ou pelo menos os pontos \emph{mais} em comum.

	Bem no começo do texto, listando questões do mito de 
	bons-pesquisadores são bons-professores -- que eu nem discordo, os meus
	pontos anteriores são mais em direção de complementar “mas nem por isso 
	não são indispensáveis em uma instituição de pesquisa” -- temos estes 
	pontos aqui:
	\begin{enumerate} 
		\item 
		\textit{Sound knowledge does not necessarily mean active 
			research}.\\
		Sim, inclusive muitas vezes professores universitários 
		“comem bola” e cometem erros conceituais, além dos evidentes
		equívocos didáticos e pedagógicos. Isto é ponto pacificado.

		\item
		\textit{The majority of mathematics courses do not include 
			advanced results reached in recent decades}.\\
		Sim, mas isso é mais uma pena do que um contra-argumento. 
		A matemática moderna facilitou a exposição e raciocínio, além
		de aprofundar nos seus temas e descobrir temas novos. É nada
		mais do que deprimente ver meia dúzia de disciplinas espalhadas
		que parecem ter parado na década de 50 chamadas de “graduação”.

		O esforço deveria ser de integrar estes conteúdos, e, 
		francamente, reformar por completo a grade curricular e as 
		ementas das disciplinas. O século passado foi -- e este também 
		tem chance de ser -- muito eventuoso na matemática, ele demanda
		revisão do ensino superior.
	\end{enumerate}

	Eu concordo que, na medida que é função de um professor ensinar, ele 
	deve estar preparado para ensinar. Mas como eu estabeleci anteriormente
	-- esta não é a única função, nem a função central, de um professor. 
	Resta então o \emph{problema} do ensino de matemática, que acredito ser
	melhor resolvido construíndo, especialmente no primeiro ano, a 
	compreensão de que o grosso do aprendizado é feito longe do professor; 
	e construír nos docentes a compreensão que pesquisa é fita próxima dos 
	alunos.

	Como o autor sugere, o aprendizado (em geral, mas particularmente de) 
	matemática não é livre de contexto; não que isto signifique que os 
	conteúdos devam estar submetidos a “\textit{interest, special needs, 
	context}. Penso que há uma medida de generalismo necessário em cada 
	área e etapa da vida; no colegial é necessário ter história \emph{e} 
	matemática, física \emph{e} filosofia; na universidade é necessário ter 
	temas que não se relacionam com os interesses imediatos dos alunos, até 
	porque: para que se especializar tão cedo? 

	Quando eu ingressei minha graduação e tinha interesse em teoria de 
	números (largamente porque era a única área da matemática superior 
	que eu de fato sabia a respeito) mas eu vim a transitar em áreas bem 
	diferentes e acabei muito longe de onde eu comecei. Se eu tivesse 
	feito uma graduação especializada na área, eu teria ficado preso na 
	área e seria consideravelmente mais infeliz.

	Meu interesse inicial em teoria de números se relaciona com o fato da 
	sau história ser muito rica e interessante\footnote{
		Mas cheia de \(\log(\log(\log\cdots))\).
	}; eu sinto que eu e meus colegas apreciamos menos o contexto histórico
	dos desenvolvimentos matemáticos, especialmente quando eles são 
	conteúdos muito remotos e já pacificados, como a \emph{grande} 
	revolução que foi a adoção do \(0\). As excessões são, claro, 
	resultados revolucionários -- impacificáveis -- como o teorema Egrégio 
	de Gauß ou toda a teoria de Galois.

	\begin{quote}
		…a considerable part of the time is devoted to the transference 
		from the notes of the lecturer to the notepads of the students 
		of relatively straightforward factual material
	\end{quote}
	eu adoro esta passagem, eu admito que me preenche de satisfação de 
	estar vindicado porque isto é algo que eu sempre senti. Mas isto é uma
	outra conversa: boa parte dos meus cadernos de disciplinas foram 
	-- ao longo da minha vida -- efetivamente cadernos de desenhos.

	Um aspecto interessante do texto é 
	\textit{fine multimedia and educational materials, virtual projects};
	o texto foi escrito em 2017, e até então não havíamos forçado e 
	estressado estas possibilidades. Eu acredito, por experiência própria e
	dados puramente anedotais, que não estamos no ponto onde podemos dizer
	\begin{quote}
		These tools can be incorporated into multimedia classrooms, 
		computer labs and virtual campuses, and allow us to combine 
		synchronic and asynchronic communications, presentations and 
		interaction.
	\end{quote}
	Especialmente a combinação do síncrono e asíncrono, misturados com 
	conexões de internet intermitentes, microfones péssimos, intromissões 
	de familiares e animais de estimação, a obra do vizinho. 

	E mais, os problemas de ensino de matemática não se dão por conta de 
	falta de ferramentas de ensino. É, sim e claro, muito legal ter acesso
	a um programa que avalie e desenhe as derivadas de uma função, ou nos 
	permita visualizar estatísticas. Eu adoro o Geogebra para a exploração 
	de construções geométricas, mais até que para a renderização de 
	gráficos de funções; a exploração gráfica e visual e sobretudo espacial 
	é vital para a compreensão dos temas mais diversos: nos apoiamos em 
	metáforas espaciais na matemática avançada constantemente, e quanto 
	mais inventivas as tecnologias com este efeito tanto melhor será a 
	\emph{nossa} vida. 

	Mas infelizmente isto não necessariamente se traduz para uma facilidade 
	de aprendizado, porque uma nova ferramenta expositiva exige também uma 
	nova qualifiação dos professores para que façam \emph{bom uso} dela. 
	Para quem está tendo dificuldade de entender, a representação pode se 
	tornar uma muleta; para quem está tendo dificuldade de ensinar, a 
	representação pode se tornar uma desculpa. Derivadas não são secantes, 
	fractais não são desenhos...
	\begin{center}
		\makebox[\textwidth]{%
			\includegraphics[width=\paperwidth]{../imgs/frac.png}%
		}
	\end{center}
	...mas muitas vezes o que dá gosto são os desenhos.
	\marginpar{\makebox[4cm]{(imagem: \cite{Jose21})}}
	Estas representações são fundamentais para inspirar o estudo -- coisa 
	que Claudi menciona, nossos temas são muito mal-motivados e não atiçam 
	em muitos a vontade de aprender --, para entender \emph{melhor} um 
	tema ou buscar melhor explicar. Mas Euler tinha uma relação muito 
	mais íntima com funções \(\mathbb R\to\mathbb R\) do que eu tenho mesmo
	com todos os meus apetrechos. Só o trabalho de grafar funções e estudar
	o seu comportamento -- local, assintótico, etc -- e sua análise 
	qualitativa no colégio já é de imensa importância.

	Talvez o mais curioso da minha leitura do texto seja o fato que eu não 
	poderia ter escrito a conclusão de maneira diferente do autor. Apesar 
	de todos os pontos que levantei, ambos acreditamos que
	\begin{quote}
		Teachers and students are, before all else, human beings with 
		brains and hearts. They have the opportunity to share 
		experiences and enthusiasm, to learn from each other. 
		Motivation, attitudes, feelings, guidance, discovery… these are 
		the words that must fill our teaching job. We have the 
		opportunity to transmit our passion for mathematics and this is 
		something that all students may remember. We do not need to be 
		actors or actresses in a mathematical play but rather the 
		partners in a rewarding journey.
	\end{quote}
	e isso informa a nossa concepção compartilhada de o que é ser um bom 
	professor. Semelhantemente ao que o nosso colega Juan tentou defender 
	a sua maneira acredito que na aula do dia 7/10/2021, o ponto principal
	-- aparentemente até na visão do autor -- da atividade do ensino é 
	acolher, instigar e promover os alunos a pesquisadores. 

	A nossa discrepância vem, muito provavelmente de diferenças em 
	experiência: eu e Juan\footnote{
		Juan inclusive evadia a aula que ele tinha no horário para 
		assistir uma ou outra aula dele, porque na época -- salvo 
		engano -- ele não estava nem no curso de matemática pura, muito
		menos matriculado naquela disciplina.
	} fomos acolhidos instigados e promovidos por um 
	professor que -- com todo o respeito devido a ele -- não é descrito por 
	muitos como um “bom didata”\footnote{
		Curiosamente, ele é um dos nossos melhores professores na minha
		opinião. Porque ele está sempre disposto a tomar um tempo 
		durante e após a aula para explicar mesmo pré-requisitos 
		básicos e é inevitável que se aprenda muito com ele. Talvez por 
		isso as turmas de início da graduação com ele são vazias, e as 
		turmas do final e da pós são relativamente cheias.

		Sob o risco de explodir o rodapé, os alunos que vieram a evadir
		a aula de álgebra linear tinham uma coisa em comum, eles não
		fizeram perguntas ou tentaram se relacionar com o resto dos 
		colegas que acabaram por sobrar -- uma pena, porque a aula foi
		riquíssima, mesmo que um pouco \emph{desnorteante}.
	}, em uma turma de álgebra linear que havia 
	efetivamente se esvaziado salvo alguns sobreviventes: uma falha
	patente na opinião do autor. E, de fato, tem algo de triste nele não 
	ter atingido todos os alunos possíveis, mas é difícil responsabilizar
	uma pessoa bem intencionada e bem disposta pelo desinteresse ou 
	timidez dos outros.

	Há algo a ser dito sobre o título, pois exatamente o que constitui um
	professor estimulante depende, claro, do estimulado. Tanto por isso eu
	preciso apresentar a minha visão enviesada e demasiadamente particular
	que eu tenho sobre o assunto.

	\begin{thebibliography}{mmmm}
		\bibitem [\bf Baez21] {Baez21}
			Publicação de John Baez no blog-colaborativo 
			\textit{n-Category Café}, disponível em
			\url{https://golem.ph.utexas.edu/category/2021/01/problems_at_the_university_of.html}
			acesso em 13/10/2021.
		\bibitem [\bf José 2021] {Jose21}
			Eu que fiz. Eu também fiz o programa que produziu a 
			imagem. 
			\url{https://gitlab.com/josealvim/FraC}

			O fractal em questão é o chamado “burning ship” que é 
			um primo próximo do conjunto de Mandelbrot, os 
			específicos da imagem estão no exemplo do 
			\texttt{READ.md} da página do projeto acima. A imagem
			foi subsequentemente tratada para ficar mais bonita.
	\end{thebibliography}

\end{document}
