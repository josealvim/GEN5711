\documentclass{article}
\usepackage{amsfonts, amsmath, amsthm}
\usepackage[portuguese]{babel}
\usepackage{pgf,tikz}
\usetikzlibrary{arrows}
\definecolor{qqqqff}{rgb}{0,0,1}
\definecolor{xdxdff}{rgb}{0.49,0.49,1}
\definecolor{uququq}{rgb}{0.25,0.25,0.25}
\newtheoremstyle{standard}	% name
	{3pt}					% space above
	{1em}					% space below
	{\normalfont}			% body font
	{0pt}					% head indent
	{\bfseries}				% head font
	{:\newline}				% after-head text
	{0em}					% after-head space
	{%
		\thmname{#1}
		\thmnumber{ #2}%
		\thmnote{: #3%
			\addcontentsline{toc}{subsubsection}{%
				#1: #3%
			}%
		}%
	}	% head spec
\theoremstyle {standard}

\newtheorem	{theorem}	{Teorema}
\newtheorem	{lemma}		{Lema}	
\newtheorem	{proposition}	{Proposição}
\newtheorem	{definition}	{Definição}
\newtheorem	{remark}	{Observação}

\begin{document}
	\title{Problema de Bisseção}
	\author{
                José Goudet Alvim\\
                \textnumero{}USP 9793968\\
                \texttt{jose.alvim@usp.br}
	}

	\maketitle 

	\begin{center}
		\textbf{Problema:}\\
		Bissecionar um ângulo cujo vértice está fora de alcance.
	\end{center}

	Efeito do enunciado, somos induzidos a crer que dispomos de um par de 
	segmentos cujas retas associadas são concorrentes mas, em virtude de 
	seu comprimento, não se encontram. Novamente por efeito do enunciado 
	somos motivados a considerar que pelo inconveniente da excessiva 
	finitude (ou seja, pequeneza) do nosso papel não podemos simplesmente
	estender as retas.

	Nosso objetivo é então construír a bissecção do ângulo entre segmentos
	dentro do retângulo mínimo que os contém, porque esta é a menor 
	dimensão de um papel que venha a conter os tais segmentos que temos em
	mãos.

	Vamos gerar imagens para o auxílio de um eventual leitor que não vá 
	seguir os passos da construção com uma régua e compasso por conta 
	própria. O programa Geogebra exporta para \LaTeX\footnote{
		Correção: esporadicamente o geogebra decidia exportar as retas
		todas erradas, em escalas esdrúxulas, e eu estava tendo que 
		ajustar a posição do nome dos pontos manualmente -- que 
		efetivamente significa ficar recompilando o \texttt{pdf} 
		mudando números semi-arbitrariamente e em um dado momento eu
		só desisti; então perdoe a tipografia dos desenhos.
	}, o que é ótimo pois é
	o que eu uso exclusivamente. Assim disposto, o problema se torna 
	encontrar a bissecção dentro do menor retângulo que contenha os 
	segmentos.

	Dispomos de dois segmentos: \(\overline{AB}\) e \(\overline{CD}\) 
	cujas retas associadas são concorrentes (e não idênticas); o objetivo 
	é construír a bissecção de um dos ângulos menores que um ângulo raso
	formados por estes dois segmentos com uma construção restrita no seu 
	espaço. Especificamente, se dispusermos destes segmentos dentro de 
	-- digamos -- um dado retângulo, desejamos que a construção possa ser
	efetuada dentro deste retângulo. 

	Faremos mais do que o desejado porque não há porquê não o fazer. 
	Provaremos que é possível fazer esta construção dentro do envólucro 
	convexo dos dois segmentos. 

	O envólucro convexo dos segmentos é dado pelo polígono convexo máximo 
	cujas arestas estão contidas em no seguinte conjunto de segmentos 
	\[
		\{
			\overline{BD},
			\overline{AB},
			\overline{BC},
			\overline{CD},
			\overline{DA}
		\}
	\]
	Vamos, como é costumeiro, dividir o problema até que ele fique 
	completamente irreconhecível e -- mais importante -- fácil de resolver.

	Com este objetivo, consideremos: se os segmentos se encontram, então 
	neste encontro -- digamos \(E\) -- podemos centrar um círculo de raio 
	\(r\); este círculo se encontra com os segmentos dados em ao menos um 
	ponto cada -- digamos \(F\) e \(G\); centrados nestes deixe dois 
	círculos tocando \(E\), ou seja, de raio \(r\). 

	Estes círculos conhecidamente se encontram em dois pontos, \(E\) e um 
	outro \(H\). Como o raio é controlado por nós, e o comprimento do 
	segmento \(\overline{EH}\) é o comprimento da diagnoal do quadrado de
	lado \(r\), então podemos escolher um raio para o qual \(H\) está de 
	fato dentro do envólucro de \(\overline{AB}\) e \(\overline{CD}\), 
	\emph{viz}.
	\[
	\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=2.0cm,y=2.0cm]
		\clip(-2.22,-0.29) rectangle (1.48,2.5);
		\draw (-1.48,2.32)-- (-1.55,0.06);
		\draw (-2.02,0.97)-- (1.32,-0.06);
		\draw(-1.53,0.82) circle (2*0.38cm);
		\draw(-1.52,1.2) circle (2*0.38cm);
		\draw(-1.16,0.71) circle (2*0.38cm);
		\draw [dotted,domain=-2.22:1.48] plot(\x,{(--0.71--0.27*\x)/0.37});
		\begin{scriptsize}
			\fill [color=qqqqff] (-2.02,0.97) circle (1.5pt);
			\draw[color=qqqqff] (-1.99,1.1) node {$A$};
			\fill [color=qqqqff] (1.32,-0.06) circle (1.5pt);
			\draw[color=qqqqff] (1.4,0) node {$B$};
			\fill [color=qqqqff] (-1.48,2.32) circle (1.5pt);
			\draw[color=qqqqff] (-1.44,2.39) node {$C$};
			\fill [color=qqqqff] (-1.55,0.06) circle (1.5pt);
			\draw[color=qqqqff] (-1.45,0.12) node {$D$};
		\end{scriptsize}
	\end{tikzpicture}
	\]
	que claramente não depende de \(A\) e \(D\) não coincidirem. Coloquemos 
	isto em um lema:
	\begin{lemma}\it
		Se dois segmentos se encontram, podemos construír sua bissecção
		dentro do seu envólucro convexo.
	\end{lemma}

	Vamos supor, então, que os segumentos não se encontrem, quais são os 
	casos? O envólucro convexo deles pode ser um triângulo ou não. Se for 
	um triângulo, há duas possibilidades: um dos vértices \(A\), \(B\),
	\(C\) ou \(D\) está no interior do triângulo; ou os segmentos são 
	subsegmentos de arestas do triângulo. No segundo caso, estendemos os 
	segmentos para que eles se encontrem, pelo lema anterior, já está 
	resolvido. Agora vamos supor que um dos vértices esteja no interior
	do triângulo.

	É imediato que ambos segmentos não podem ter vértices dentro do 
	triângulo, e é claro que o segmento que não tem vértice interior ao 
	triângulo é uma aresta; e é claro que o vértice restante é vértice 
	inclusive \emph{do} triângulo. Isto é, temos em mãos algo assim -- 
	sem perda de generalidade com a nomeação dos vértices:
	\[
		\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm]
			\clip(-9.97,-2.1) rectangle (-3.26,2.1);
			\draw (-7.11,-0.24)-- (-8.13,1.49);
			\draw (-3.96,-0.76)-- (-9.08,-1.29);
			\draw [dotted] (-9.08,-1.29)-- (-8.13,1.49);
			\draw [dotted] (-9.08,-1.29)-- (-7.11,-0.24);
			\draw [dotted] (-3.96,-0.76)-- (-8.13,1.49);
			\draw [dotted] (-7.11,-0.24)-- (-3.96,-0.76);
			\begin{scriptsize}
				\fill [color=qqqqff] (-7.11,-0.24) circle (1.5pt);
				\draw [color=qqqqff] (-6.99,-0.04) node {$A$};
				\fill [color=qqqqff] (-8.13,1.49 ) circle (1.5pt);
				\draw [color=qqqqff] (-8,1.7     ) node {$B$};
				\fill [color=qqqqff] (-3.96,-0.76) circle (1.5pt);
				\draw [color=qqqqff] (-3.84,-0.55) node {$C$};
				\fill [color=qqqqff] (-9.08,-1.29) circle (1.5pt);
				\draw [color=qqqqff] (-8.82,-0.94) node {$D$};
			\end{scriptsize}
		\end{tikzpicture}
	\]
	Extende-se a reta do segmento ao qual pertence o ponto que é interior 
	ao triângulo; esta reta deve pois encontrar-se com a aresta opositora, 
	que é o outro segmento.
	\[
	\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm]
		\clip(-9.66,-1.58) rectangle (-3.05,1.95);
		\draw (-7.11,-0.24)-- (-8.13,1.49);
		\draw (-3.96,-0.76)-- (-9.08,-1.29);
		\draw [dotted] (-9.08,-1.29)-- (-8.13,1.49);
		\draw [dotted] (-9.08,-1.29)-- (-7.11,-0.24);
		\draw [dotted] (-3.96,-0.76)-- (-8.13,1.49);
		\draw [dotted] (-7.11,-0.24)-- (-3.96,-0.76);
		\draw [domain=-9.66:-3.05] plot(\x,{(-12.58-1.73*\x)/1.02});
		\begin{scriptsize}
			\fill [color=qqqqff] (-7.11,-0.24) circle (1.5pt);
			\draw[color=qqqqff] (-6.99,-0.04) node {$A$};
			\fill [color=qqqqff] (-8.13,1.49) circle (1.5pt);
			\draw[color=qqqqff] (-8,1.7) node {$B$};
			\fill [color=qqqqff] (-3.96,-0.76) circle (1.5pt);
			\draw[color=qqqqff] (-3.84,-0.55) node {$C$};
			\fill [color=qqqqff] (-9.08,-1.29) circle (1.5pt);
			\draw[color=qqqqff] (-8.82,-0.94) node {$D$};
		\end{scriptsize}
	\end{tikzpicture}
	\]
	No ponto de seu encontro -- digamos \(E\) -- traçar um semi-círculo 
	\(c\) de raio \(r\) qualquer, só usaremos a parte interna ao triângulo 
	dele, então nossa construção ainda é permissível nas restrições do 
	problema.
	\[
	\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm]
		\clip(-9.9,-1.7) rectangle (-3.16,2.3);
		\draw (-7.11,-0.24)-- (-8.13,1.49);
		\draw (-3.96,-0.76)-- (-9.08,-1.29);
		\draw [dotted] (-9.08,-1.29)-- (-8.13,1.49);
		\draw [dotted] (-9.08,-1.29)-- (-7.11,-0.24);
		\draw [dotted] (-3.96,-0.76)-- (-8.13,1.49);
		\draw [dotted] (-7.11,-0.24)-- (-3.96,-0.76);
		\draw [domain=-9.9:-3.16] plot(\x,{(-12.58-1.73*\x)/1.02});
		\draw [shift={(-6.65,-1.03)}] plot[domain=0.1:3.24,variable=\t]({1*0.73*cos(\t r)+0*0.73*sin(\t r)},{0*0.73*cos(\t r)+1*0.73*sin(\t r)});
		\begin{scriptsize}
			\fill [color=qqqqff] (-7.11,-0.24) circle (1.5pt);
			\draw[color=qqqqff] (-6.97,-0.03) node {$A$};
			\fill [color=qqqqff] (-8.13,1.49) circle (1.5pt);
			\draw[color=qqqqff] (-7.99,1.71) node {$B$};
			\fill [color=qqqqff] (-3.96,-0.76) circle (1.5pt);
			\draw[color=qqqqff] (-3.83,-0.54) node {$C$};
			\fill [color=qqqqff] (-9.08,-1.29) circle (1.5pt);
			\draw[color=qqqqff] (-8.8,-0.91) node {$D$};
			\fill [color=uququq] (-6.65,-1.03) circle (1.5pt);
			\draw[color=uququq] (-6.52,-0.81) node {$E$};
		\end{scriptsize}
	\end{tikzpicture}
	\]
	Então a bissecção é imediata: traçar dois círculos que se encontram no 
	vértice \(E\) centrados um onde \(c\) encontra-se com \(\overline{AB}\) 
	e o outro onde \(c\) encontra-se com \(\overline{CD}\) -- aqui há uma 
	escolha de pontos, ambas darão bissecções, dos dois ângulos 
	suplementares entre os segmentos dados.
	\[
	\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm]
		\clip(-9.36,-2.11) rectangle (-3.38,2.04);
		\draw (-7.11,-0.24)-- (-8.13,1.49);
		\draw (-3.96,-0.76)-- (-9.08,-1.29);
		\draw [dotted] (-9.08,-1.29)-- (-8.13,1.49);
		\draw [dotted] (-9.08,-1.29)-- (-7.11,-0.24);
		\draw [dotted] (-3.96,-0.76)-- (-8.13,1.49);
		\draw [dotted] (-7.11,-0.24)-- (-3.96,-0.76);
		\draw [domain=-9.36:-3.38] plot(\x,{(-12.58-1.73*\x)/1.02});
		\draw [shift={(-6.65,-1.03)}] plot[domain=0.1:3.24,variable=\t]({1*0.73*cos(\t r)+0*0.73*sin(\t r)},{0*0.73*cos(\t r)+1*0.73*sin(\t r)});
		\draw(-5.93,-0.96) circle (0.73cm);
		\draw(-7.02,-0.41) circle (0.73cm);
		\begin{scriptsize}
		\fill [color=qqqqff] (-7.11,-0.24) circle (1.5pt);
		\fill [color=qqqqff] (-8.13,1.49) circle (1.5pt);
		\draw[color=qqqqff] (-8.04,1.62) node {$B$};
		\fill [color=qqqqff] (-3.96,-0.76) circle (1.5pt);
		\draw[color=qqqqff] (-3.87,-0.62) node {$C$};
		\fill [color=qqqqff] (-9.08,-1.29) circle (1.5pt);
		\draw[color=qqqqff] (-8.9,-1.06) node {$D$};
		\fill [color=uququq] (-6.65,-1.03) circle (1.5pt);
		\fill [color=uququq] (-5.93,-0.96) circle (1.5pt);
		\fill [color=uququq] (-7.02,-0.41) circle (1.5pt);
		\fill [color=uququq] (-6.29,-0.33) circle (1.5pt);
		\draw[color=uququq] (-6.24,-0.1) node {$I$};
		\end{scriptsize}
		\end{tikzpicture}
	\]
	Aqui temos uma questão: será que os dois encontros do círculo sempre 
	ficam dentro do triângulo? Não para todo ponto raio, mas controlamos
	\(r\), e o comprimento de \(\overline{EI}\) é proporcional a \(r\) --
	é, afinal, a diagonal do quadrado de lado \(r\) -- então isto está 
	\emph{controlado}. 

	E acabou, basta traçar a reta \(\overline{EI}\) que bissecta um dos 
	ângulos a depender de qual lado do círculo escolhemos. As duas retas 
	obtidas são, claro, perpendiculares e a outra não vou desenhar:
	\[
	\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm]
		\clip(-12.69,-4.93) rectangle (4.93,4.15);
		\draw (-7.11,-0.24)-- (-8.13,1.49);
		\draw (-3.96,-0.76)-- (-9.08,-1.29);
		%\draw [dotted] (-9.08,-1.29)-- (-8.13,1.49);
		%\draw [dotted] (-9.08,-1.29)-- (-7.11,-0.24);
		%\draw [dotted] (-3.96,-0.76)-- (-8.13,1.49);
		%\draw [dotted] (-7.11,-0.24)-- (-3.96,-0.76);
		\draw [domain=-12.69:4.93] plot(\x,{(-12.58-1.73*\x)/1.02});
		\draw [shift={(-6.65,-1.03)}] plot[domain=0.1:3.24,variable=\t]({1*0.73*cos(\t r)+0*0.73*sin(\t r)},{0*0.73*cos(\t r)+1*0.73*sin(\t r)});
		\draw(-5.93,-0.96) circle (0.73cm);
		\draw(-7.02,-0.41) circle (0.73cm);
		\draw [dotted, domain=-12.69:4.93] plot(\x,{(--4.3--0.7*\x)/0.36});
		\begin{scriptsize}
			\fill [color=qqqqff] (-7.11,-0.24) circle (1.5pt);
			\draw[color=qqqqff] (-7.04,-0.11) node {$A$};
			\fill [color=qqqqff] (-8.13,1.49) circle (1.5pt);
			\draw[color=qqqqff] (-8.04,1.63) node {$B$};
			\fill [color=qqqqff] (-3.96,-0.76) circle (1.5pt);
			\draw[color=qqqqff] (-3.88,-0.62) node {$C$};
			\fill [color=qqqqff] (-9.08,-1.29) circle (1.5pt);
			\draw[color=qqqqff] (-8.91,-1.07) node {$D$};
		\end{scriptsize}
	\end{tikzpicture}
	\]

	Podemos colocar isto em um outro lema
	\begin{lemma}\it
		Se dois segmentos tem como envólucro convexo um triângulo, 
		então podemos construír a bissecção do ângulo por eles formado
		dentro do dito envólucro.
	\end{lemma}

	Resta então o caso em que o envólucro não é triangular. É imediato que 
	o dito envólucro então é um quadrilátero; e claro, se ele for 
	quadrilátero, então os seguementos são lados opostos do quadrilátero. 

	Vamos quebrar o problema novamente: um ângulo pode ser 
	\emph{agudo}, \emph{reto}, \emph{obtuso}, \emph{raso}, 
	\emph{reentrante} e \emph{completo}. Mas bissectar um ângulo reentrante
	é exatamente a mesma coisa que bissectar o seu ângulo replementar, e 
	inclusive não tem como representar um reentrante sem desenhar o seu 
	replementar. Então basta que saibamos resolver o problema da bissecção
	para ângulos de nulo até raso; nulo e rasos não podem ser, pois os 
	segmentos não são paralelos.
	\\[1em]\noindent
	Tomemos então um par de segmentos cujo ângulo formado é obtuso.
	\begin{lemma}[Uma Digressão]
		Se o envólucro é quadrilátero e um dado segmento forma um 
		ângulo obtuso no nele, então neste vértice onde o ângulo é 
		obtuso podemos construír uma perpendicular ao segmento dentro 
		do envólucro.
	\end{lemma}
	\begin{proof}
		Tome um ponto a esmo no segmento, trace a perpendicular 
		do segmento no ponto e intercepte-a com o círculo centrado no 
		ponto e que atinge a extremidade que forma o ângulo obtuso; 
		sobre o ponto que repousa sobre o círculo e a perpendicular 
		construa uma paralela ao segmento original. Isto tudo pode 
		ser feito dentro do envólucro:
		\[
		\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=2.0cm,y=2.0cm]
		\clip(-2.9,-0.7) rectangle (2.06,1.99);
		\draw (-2.56,1.75)-- (-1.73,0);
		\draw (-1.17,-0.34)-- (1.79,-0.31);
		\draw [dotted] (-1.73,0)-- (-1.17,-0.34);
		\draw [domain=-2.9:2.06] plot(\x,{(--2.36--0.83*\x)/1.75});
		\draw(-1.94,0.44) circle (2*0.48cm);
		\draw [domain=-2.9:2.06] plot(\x,{(--2.1--1.75*\x)/-0.83});
		\begin{scriptsize}
		\fill [color=qqqqff] (-1.17,-0.34) circle (1.5pt);
		\draw[color=qqqqff] (-1.14,-0.28) node {$A$};
		\fill [color=qqqqff] (1.79,-0.31) circle (1.5pt);
		\draw[color=qqqqff] (1.82,-0.20) node {$B$};
		\fill [color=qqqqff] (-2.56,1.75) circle (1.5pt);
		\draw[color=qqqqff] (-2.52,1.85) node {$C$};
		\fill [color=qqqqff] (-1.73,0) circle (1.5pt);
		\draw[color=qqqqff] (-1.69,0.10) node {$D$};
		\fill [color=xdxdff] (-1.94,0.44) circle (1.5pt);
		\draw[color=xdxdff] (-1.9,0.55) node {$E$};
		\fill [color=uququq] (-1.5,0.64) circle (1.5pt);
		\draw[color=uququq] (-1.47,0.71) node {$F$};
		\end{scriptsize}
		\end{tikzpicture}
		\]
		
		É evidente então que podemos transportar a perpendicular sobre o 
		ponto escolhido a esmo para sobre o ponto extremal do segmento: 
		conecte o ponto extremal com o ponto repousante sobre o círculo 
		e a perpendicular, podemos dividir este segmento em dois, resultando
		no ponto central do quadrado determinado; e podemos então seguir a 
		outra diagonal para encontrar um ponto que determina a perpendicular
		sobre o extremal.
		\[
		\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=2.0cm,y=2.0cm]
		\clip(-2.95,-0.69) rectangle (1.88,2.02);
		\draw (-2.56,1.75)-- (-1.73,0);
		\draw (-1.17,-0.34)-- (1.79,-0.31);
		\draw [dotted] (-1.73,0)-- (-1.17,-0.34);
		\draw [domain=-2.95:1.88] plot(\x,{(--2.36--0.83*\x)/1.75});
		\draw(-1.94,0.44) circle (2*0.48cm);
		\draw [domain=-2.95:1.88] plot(\x,{(--2.1--1.75*\x)/-0.83});
		\draw [domain=-2.95:1.88] plot(\x,{(--1.43--0.83*\x)/1.75});
		\begin{scriptsize}
		\fill [color=qqqqff] (-1.17,-0.34) circle (1.5pt);
		\draw[color=qqqqff] (-1.14,-0.28) node {$A$};
		\fill [color=qqqqff] (1.79,-0.31) circle (1.5pt);
		\draw[color=qqqqff] (1.82,-0.20) node {$B$};
		\fill [color=qqqqff] (-2.56,1.75) circle (1.5pt);
		\draw[color=qqqqff] (-2.52,1.85) node {$C$};
		\fill [color=qqqqff] (-1.73,0) circle (1.5pt);
		\draw[color=qqqqff] (-1.69,0.10) node {$D$};
		\fill [color=xdxdff] (-1.94,0.44) circle (1.5pt);
		\draw[color=xdxdff] (-1.9,0.55) node {$E$};
		\fill [color=uququq] (-1.5,0.64) circle (1.5pt);
		\draw[color=uququq] (-1.47,0.71) node {$F$};
		\fill [color=uququq] (-1.3,0.21) circle (1.5pt);
		\draw[color=uququq] (-1.26,0.3) node {$G$};
		\end{scriptsize}
		\end{tikzpicture}
		\]

		Isto não podemos fazer quando o ângulo é reto ou mais agudo: o ponto
		cai para fora.
		\[
		\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=2.0cm,y=2.0cm]
		\clip(-2.11,-1.08) rectangle (2.2,1.91);
		\draw (-0.36,1.01)-- (-1.5,0.03);
		\draw (-0.62,-0.85)-- (1.86,-0.32);
		\draw [dotted] (-1.5,0.03)-- (-0.62,-0.85);
		\draw [domain=-2.11:2.2] plot(\x,{(-0.2-1.13*\x)/0.98});
		\draw(-0.75,0.67) circle (2*0.98cm);
		\draw [domain=-2.11:2.2] plot(\x,{(--0.02--0.98*\x)/1.13});
		\draw [domain=-2.11:2.2] plot(\x,{(-1.67-1.13*\x)/0.98});
		\begin{scriptsize}
		\fill [color=qqqqff] (-0.62,-0.85) circle (1.5pt);
		\draw[color=qqqqff] (-0.56,-0.75) node {$A$};
		\fill [color=qqqqff] (1.86,-0.32) circle (1.5pt);
		\draw[color=qqqqff] (1.92,-0.22) node {$B$};
		\fill [color=qqqqff] (-0.36,1.01) circle (1.5pt);
		\draw[color=qqqqff] (-0.31,1.1) node {$C$};
		\fill [color=qqqqff] (-1.5,0.03) circle (1.5pt);
		\draw[color=qqqqff] (-1.49,0.18) node {$D$};
		\fill [color=xdxdff] (-0.75,0.67) circle (1.5pt);
		\draw[color=xdxdff] (-0.72,0.86) node {$E$};
		\fill [color=uququq] (-0.11,-0.07) circle (1.5pt);
		\draw[color=uququq] (-0.1,0.07) node {$F$};
		\fill [color=uququq] (-0.85,-0.72) circle (1.5pt);
		\draw[color=uququq] (-1.01,-0.64) node {$G$};
		\end{scriptsize}
		\end{tikzpicture}
		\]
	\end{proof}
	
	Para que tudo isso? Uma premonição matemática de minha parte -- juro.
	\begin{lemma}
		Notemos que se o ângulo entre dois lados opostos de um quadrilátero 
		convexo for obtuso, isto é:
		\[
		\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=2.0cm,y=2.0cm]
		\clip(-4.4,1.21) rectangle (-0.66,2.93);
		\draw (-3.27,1.54)-- (-0.83,1.52);
		\draw (-3.51,1.72)-- (-4.09,2.72);
		\begin{scriptsize}
		\fill [color=qqqqff] (-3.27,1.54) circle (1.5pt);
		\draw[color=qqqqff] (-3.21,1.63) node {$A$};
		\fill [color=qqqqff] (-0.83,1.52) circle (1.5pt);
		\draw[color=qqqqff] (-0.77,1.62) node {$B$};
		\fill [color=qqqqff] (-3.51,1.72) circle (1.5pt);
		\draw[color=qqqqff] (-3.49,1.87) node {$C$};
		\fill [color=qqqqff] (-4.09,2.72) circle (1.5pt);
		\draw[color=qqqqff] (-4.03,2.81) node {$D$};
		\end{scriptsize}
		\end{tikzpicture}
		\]
		então cada um destes lados opostos forma exatamente um ângulo obtuso
		dentro do quadrilátero:
		\[
		\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=2.0cm,y=2.0cm]
		\clip(-4.32,1.17) rectangle (-0.43,3);
		\draw (-3.27,1.54)-- (-0.83,1.52);
		\draw (-3.51,1.72)-- (-4.09,2.72);
		\draw (-3.51,1.72)-- (-3.27,1.54);
		\draw (-4.09,2.72)-- (-0.83,1.52);
		\begin{scriptsize}
		\fill [color=qqqqff] (-3.27,1.54) circle (1.5pt);
		\draw[color=qqqqff] (-3.21,1.63) node {$A$};
		\fill [color=qqqqff] (-0.83,1.52) circle (1.5pt);
		\draw[color=qqqqff] (-0.77,1.62) node {$B$};
		\fill [color=qqqqff] (-3.51,1.72) circle (1.5pt);
		\draw[color=qqqqff] (-3.49,1.87) node {$C$};
		\fill [color=qqqqff] (-4.09,2.72) circle (1.5pt);
		\draw[color=qqqqff] (-4.03,2.81) node {$D$};
		\end{scriptsize}
		\end{tikzpicture}
		\]
	\end{lemma}

	Considerando os lemas acima, percebemos que estamos nas hipóteses do  
	lema anterior que nos permite aplicar o lema antes dele.
	\[
	\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=2.0cm,y=2.0cm]
	\clip(-4.29,1.3) rectangle (-0.71,2.96);
	\draw (-3.27,1.54)-- (-0.83,1.52);
	\draw (-3.51,1.72)-- (-4.13,2.8);
	\draw [dotted] (-3.51,1.72)-- (-3.27,1.54);
	\draw [domain=-4.29:-0.71] plot(\x,{(-4.03-0.62*\x)/-1.09});
	\draw [dotted] (-4.13,2.8)-- (-0.83,1.52);
	\begin{scriptsize}
	\fill [color=qqqqff] (-3.27,1.54) circle (1.5pt);
	\draw[color=qqqqff] (-3.23,1.61) node {$A$};
	\fill [color=qqqqff] (-0.83,1.52) circle (1.5pt);
	\draw[color=qqqqff] (-0.78,1.59) node {$B$};
	\fill [color=qqqqff] (-3.51,1.72) circle (1.5pt);
	\draw[color=qqqqff] (-3.5,1.83) node {$C$};
	\fill [color=qqqqff] (-4.13,2.8) circle (1.5pt);
	\draw[color=qqqqff] (-4.08,2.87) node {$D$};
	\fill [color=uququq] (-2.62,2.22) circle (1.5pt);
	\draw[color=uququq] (-2.58,2.29) node {$E$};
	\end{scriptsize}
	\end{tikzpicture}
	\]
	podemos então construír uma perpendicular a um segmento em um dos seus 
	extremos. Digamos que o ponto onde ela cruza com o envólucro -- que 
	nunca vai cair sobre o outro segmento (de outra forma eles seriam 
	paralelos) -- se chama \(E\); então isto divide o problema em dois -- 
	de certa forma -- pois temos um ângulo reto formando um triângulo 
	\(\overline{DEA}\) e temos dois segmentos \(\overline{CE}\) e 
	\(\overline{AB}\) formando um ângulo agudo.

	Admitindo que saibamos bissectar o ângulo reto, e bissectar o ângulo
	agudo, podemos juntar as soluções? Vamos tentar!
	\[
	\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=2.0cm,y=2.0cm]
	\clip(-4.31,1.23) rectangle (-0.7,2.98);
	\draw (-3.27,1.54)-- (-0.83,1.52);
	\draw (-3.51,1.72)-- (-4.13,2.8);
	\draw [dotted] (-3.51,1.72)-- (-3.27,1.54);
	\draw [dotted] (-4.13,2.8)-- (-0.83,1.52);
	\draw [dash pattern=on 1pt off 1pt,domain=-4.31:-0.7] plot(\x,{(--2.45--0.25*\x)/0.97});
	\draw [dash pattern=on 1pt off 1pt,domain=-4.31:-0.7] plot(\x,{(-3.84-0.96*\x)/-0.27});
	\draw (-3.51,1.72)-- (-2.62,2.22);
	\begin{scriptsize}
	\fill [color=qqqqff] (-3.27,1.54) circle (1.5pt);
	\draw[color=qqqqff] (-3.23,1.61) node {$A$};
	\fill [color=qqqqff] (-0.83,1.52) circle (1.5pt);
	\draw[color=qqqqff] (-0.78,1.59) node {$B$};
	\fill [color=qqqqff] (-3.51,1.72) circle (1.5pt);
	\draw[color=qqqqff] (-3.53,1.88) node {$C$};
	\fill [color=qqqqff] (-4.13,2.8) circle (1.5pt);
	\draw[color=qqqqff] (-4.08,2.87) node {$D$};
	\fill [color=uququq] (-2.62,2.22) circle (1.5pt);
	\draw[color=uququq] (-2.58,2.29) node {$E$};
	\end{scriptsize}
	\end{tikzpicture}
	\]

	Construímos uma reta \(\delta\) que é a reta que passa pelo encontro
	da bissetriz de \(\overline{CE}\) e \(\overline{AB}\) e é a soma desta
	com a metade de um ângulo reto, \textit{viz}.
	\[
	\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=2.0cm,y=2.0cm]
	\clip(-4.27,1.31) rectangle (-0.7,2.9);
	\draw (-3.27,1.54)-- (-0.83,1.52);
	\draw (-3.51,1.72)-- (-4.13,2.8);
	\draw [dotted] (-3.51,1.72)-- (-3.27,1.54);
	\draw [dotted] (-4.13,2.8)-- (-0.83,1.52);
	\draw (-3.51,1.72)-- (-2.62,2.22);
	\draw [dash pattern=on 1pt off 1pt on 1pt off 4pt,domain=-4.27:-0.7] plot(\x,{(--3.78--0.86*\x)/0.51});
	\begin{scriptsize}
	\fill [color=qqqqff] (-3.27,1.54) circle (1.5pt);
	\draw[color=qqqqff] (-3.24,1.58) node {$A$};
	\fill [color=qqqqff] (-0.83,1.52) circle (1.5pt);
	\draw[color=qqqqff] (-0.8,1.56) node {$B$};
	\fill [color=qqqqff] (-3.51,1.72) circle (1.5pt);
	\draw[color=qqqqff] (-3.59,1.78) node {$C$};
	\fill [color=qqqqff] (-4.13,2.8) circle (1.5pt);
	\draw[color=qqqqff] (-4.1,2.85) node {$D$};
	\fill [color=uququq] (-2.62,2.22) circle (1.5pt);
	\draw[color=uququq] (-2.6,2.26) node {$E$};
	\fill [color=uququq] (-3.41,1.65) circle (1.5pt);
	\end{scriptsize}
	\end{tikzpicture}
	\]
	esta reta é forma ângulos idênticos com os dois segmentos, mas ela
	\emph{não é} a bissetriz, ela é apenas paralela à reta que buscamos.
	\[
	\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=2.0cm,y=2.0cm]
	\clip(-4.21,1.38) rectangle (-0.75,2.89);
	\draw (-3.27,1.54)-- (-0.83,1.52);
	\draw (-3.51,1.72)-- (-4.13,2.8);
	\draw [dotted] (-3.51,1.72)-- (-3.27,1.54);
	\draw [dotted] (-4.13,2.8)-- (-0.83,1.52);
	\draw (-3.51,1.72)-- (-2.62,2.22);
	\draw [dash pattern=on 1pt off 1pt on 1pt off 4pt,domain=-4.21:-0.75] plot(\x,{(--3.78--0.86*\x)/0.51});
	\draw [domain=-4.21:-0.75] plot(\x,{(--3.72--0.86*\x)/0.51});
	\begin{scriptsize}
	\fill [color=qqqqff] (-3.27,1.54) circle (1.5pt);
	\draw[color=qqqqff] (-3.24,1.58) node {$A$};
	\fill [color=qqqqff] (-0.83,1.52) circle (1.5pt);
	\draw[color=qqqqff] (-0.81,1.56) node {$B$};
	\fill [color=qqqqff] (-3.51,1.72) circle (1.5pt);
	\draw[color=qqqqff] (-3.58,1.77) node {$C$};
	\fill [color=qqqqff] (-4.13,2.8) circle (1.5pt);
	\draw[color=qqqqff] (-4.1,2.84) node {$D$};
	\fill [color=uququq] (-2.62,2.22) circle (1.5pt);
	\draw[color=uququq] (-2.6,2.26) node {$E$};
	\fill [color=uququq] (-3.41,1.65) circle (1.5pt);
	\end{scriptsize}
	\end{tikzpicture}
	\]
	Escolhemos um ponto em \(\delta\) a esmo, construímos a perpendicular
	sobre o ponto e esta encontra com os segmentos originais; sobre estes 
	pontos construímos círculos ligando-os ao ponto escolhido a esmo. Um
	destes círculos é maior que o outro -- mesmo que marginalmente, se não
	o fosse, \(\delta\) coincide com a bissetriz e já estávamos resolvidos.
	\[
	\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=2.0cm,y=2.0cm]
	\clip(-4.2,1.13) rectangle (-0.76,2.87);
	\draw (-3.27,1.54)-- (-0.83,1.52);
	\draw (-3.51,1.72)-- (-4.13,2.8);
	\draw [dotted] (-3.51,1.72)-- (-3.27,1.54);
	\draw [dotted] (-4.13,2.8)-- (-0.83,1.52);
	\draw (-3.51,1.72)-- (-2.62,2.22);
	\draw [dash pattern=on 1pt off 1pt on 1pt off 4pt,domain=-4.2:-0.76] plot(\x,{(--3.78--0.86*\x)/0.51});
	\draw [domain=-4.2:-0.76] plot(\x,{(--0.15--0.51*\x)/-0.86});
	\draw(-3.65,1.97) circle (0.74cm);
	\draw(-2.91,1.54) circle (0.98cm);
	\begin{scriptsize}
	\fill [color=qqqqff] (-3.27,1.54) circle (1.5pt);
	\draw[color=qqqqff] (-3.24,1.58) node {$A$};
	\fill [color=qqqqff] (-0.83,1.52) circle (1.5pt);
	\draw[color=qqqqff] (-0.81,1.56) node {$B$};
	\fill [color=qqqqff] (-3.51,1.72) circle (1.5pt);
	\draw[color=qqqqff] (-3.58,1.77) node {$C$};
	\fill [color=qqqqff] (-4.13,2.8) circle (1.5pt);
	\draw[color=qqqqff] (-4.1,2.84) node {$D$};
	\fill [color=uququq] (-2.62,2.22) circle (1.5pt);
	\draw[color=uququq] (-2.6,2.26) node {$E$};
	\fill [color=uququq] (-3.41,1.65) circle (1.5pt);
	\fill [color=xdxdff] (-3.33,1.78) circle (1.5pt);
	\fill [color=uququq] (-3.65,1.97) circle (1.5pt);
	\fill [color=uququq] (-2.91,1.54) circle (1.5pt);
	\end{scriptsize}
	\end{tikzpicture}
	\]
	Esqueçamos o círculo pequeno, e com nosso compasso, vamos construír no
	ponto do círculo pequeno um outro do raio do grande. Isto determina um
	ponto no interior do quadrilátero -- se escolhermos um ponto a esmo 
	\emph{bom}; 
	\[
	\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=2.0cm,y=2.0cm]
	\clip(-4.21,1.15) rectangle (-0.75,2.92);
	\draw (-3.27,1.54)-- (-0.83,1.52);
	\draw (-3.51,1.72)-- (-4.13,2.8);
	\draw [dotted] (-3.51,1.72)-- (-3.27,1.54);
	\draw [dotted] (-4.13,2.8)-- (-0.83,1.52);
	\draw (-3.51,1.72)-- (-2.62,2.22);
	\draw [dash pattern=on 1pt off 1pt on 1pt off 4pt,domain=-4.21:-0.75] plot(\x,{(--3.78--0.86*\x)/0.51});
	\draw [domain=-4.21:-0.75] plot(\x,{(--0.15--0.51*\x)/-0.86});
	\draw(-2.91,1.54) circle (0.98cm);
	\draw(-3.65,1.97) circle (0.98cm);
	\begin{scriptsize}
	\fill [color=qqqqff] (-3.27,1.54) circle (1.5pt);
	\draw[color=qqqqff] (-3.24,1.58) node {$A$};
	\fill [color=qqqqff] (-0.83,1.52) circle (1.5pt);
	\draw[color=qqqqff] (-0.81,1.56) node {$B$};
	\fill [color=qqqqff] (-3.51,1.72) circle (1.5pt);
	\draw[color=qqqqff] (-3.58,1.77) node {$C$};
	\fill [color=qqqqff] (-4.13,2.8) circle (1.5pt);
	\draw[color=qqqqff] (-4.1,2.84) node {$D$};
	\fill [color=uququq] (-2.62,2.22) circle (1.5pt);
	\draw[color=uququq] (-2.6,2.26) node {$E$};
	\fill [color=uququq] (-3.41,1.65) circle (1.5pt);
	\fill [color=xdxdff] (-3.33,1.78) circle (1.5pt);
	\fill [color=uququq] (-3.65,1.97) circle (1.5pt);
	\fill [color=uququq] (-2.91,1.54) circle (1.5pt);
	\end{scriptsize}
	\end{tikzpicture}
	\]
	transportamos a reta \(\delta\) para o ponto construído -- não vou 
	mostrar porque isto pode sempre ser feito dentro da área, fica um 
	mistério -- a reta resultante é equidistante dos pontos obtidos 
	pelo encontro suas perpendiculares com os segmentos; que é uma 
	propriedade que só a reta da bissetriz possui.
	\[
	\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=2.0cm,y=2.0cm]
	\clip(-4.23,1.42) rectangle (-0.73,2.9);
	\draw (-3.27,1.54)-- (-0.83,1.52);
	\draw (-3.51,1.72)-- (-4.13,2.8);
	\draw [dotted] (-3.51,1.72)-- (-3.27,1.54);
	\draw [dotted] (-4.13,2.8)-- (-0.83,1.52);
	\draw [dash pattern=on 1pt off 1pt on 1pt off 4pt,domain=-4.23:-0.73] plot(\x,{(--3.78--0.86*\x)/0.51});
	\draw [domain=-4.23:-0.73] plot(\x,{(--3.72--0.86*\x)/0.51});
	\begin{scriptsize}
	\fill [color=qqqqff] (-3.27,1.54) circle (1.5pt);
	\draw[color=qqqqff] (-3.24,1.58) node {$A$};
	\fill [color=qqqqff] (-0.83,1.52) circle (1.5pt);
	\draw[color=qqqqff] (-0.81,1.56) node {$B$};
	\fill [color=qqqqff] (-3.51,1.72) circle (1.5pt);
	\draw[color=qqqqff] (-3.58,1.77) node {$C$};
	\fill [color=qqqqff] (-4.13,2.8) circle (1.5pt);
	\draw[color=qqqqff] (-4.1,2.84) node {$D$};
	\fill [color=uququq] (-3.41,1.65) circle (1.5pt);
	\fill [color=uququq] (-3.16,1.96) circle (1.5pt);
	\end{scriptsize}
	\end{tikzpicture}
	\]

	Botemos isto em um lema
	\begin{lemma}\it
		Se o ângulo formado por dois segmentos cujo envólucro convexo
		for obtuso, e se formos capazes de bissectar nas áreas 
		apropriadas os ângulos reto e agudo que vimos anteriormente,
		então somos capazes de bissectar o ângulo de maneira 
		satisfatória.
	\end{lemma}

	É imediato -- haja visto que são 3h58 da manhã pois eu achei que isto 
	ia ser bem mais trivial do que foi -- que podemos bissectar um ângulo 
	reto dentro de um triângulo reto; resta, para provarmos o desejado, 
	mostrarmos que somos capazes de -- dados dois segmentos com envólucro
	quadrilateral que formam um ângulo agudo -- bissectá-lo.

	A vantagem de fazer isso com um ângulo agudo, é que podemos construír
	alturas que de fato encontram o outro segmento: usando aquele lema 
	da digressão vamos colocar uma perpendicular a um segmento em uma 
	de suas extremidades; onde esta perpendicular encontra o segmento 
	oposto chamamos de \(E\), sobre \(E\) construímos uma perpendicular
	do seu segmento bissectamos o ângulo formado entre estas duas retas 
	construídas:
	\[
	\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=.5cm,y=.5cm]
	\clip(-3.1,-2.64) rectangle (9.68,7.94);
	\draw (0.54,-2.06)-- (9.04,-1.12);
	\draw (-2,1.2)-- (2.36,7.2);
	\draw [dash pattern=on 6pt off 6pt] (-2,1.2)-- (0.54,-2.06);
	\draw [domain=-3.1:9.68] plot(\x,{(-2.65--8.5*\x)/-0.94});
	\draw [domain=-3.1:9.68] plot(\x,{(-954.18--186.17*\x)/-256.2});
	\draw [dash pattern=on 3pt off 3pt,domain=-3.1:9.68] plot(\x,{(-1.82--0.86*\x)/-0.5});
	\begin{scriptsize}
	\fill [color=qqqqff] (-2,1.2) circle (1.5pt);
	\draw[color=qqqqff] (-2.06,1.62) node {$A$};
	\fill [color=qqqqff] (0.54,-2.06) circle (1.5pt);
	\draw[color=qqqqff] (0.7,-1.8) node {$C$};
	\fill [color=qqqqff] (2.36,7.2) circle (1.5pt);
	\draw[color=qqqqff] (2.52,7.46) node {$B$};
	\fill [color=qqqqff] (9.04,-1.12) circle (1.5pt);
	\draw[color=qqqqff] (9.2,-0.86) node {$D$};
	\fill [color=uququq] (-0.11,3.8) circle (1.5pt);
	\draw[color=uququq] (0.06,4.4) node {$E$};
	\end{scriptsize}
	\end{tikzpicture}
	\]
	o ponto médio entre \(E\) e o ponto onde a bissetriz encontra 
	\(\overline{CD}\) está na bissecção do ângulo desejado, e a bissetriz 
	construída é perpendicular à reta que buscamos, então basta construír
	uma perpendicular sobre este ponto médio:
	\[
	\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=0.5cm,y=0.5cm]
	\clip(-3.76,-3.78) rectangle (9.76,8.06);
	\draw (0.54,-2.06)-- (9.04,-1.12);
	\draw (-2,1.2)-- (2.36,7.2);
	\draw [dash pattern=on 2pt off 2pt] (-2,1.2)-- (0.54,-2.06);
	\draw [domain=-3.76:9.76] plot(\x,{(-2.65--8.5*\x)/-0.94});
	\draw [domain=-3.76:9.76] plot(\x,{(-954.18--186.17*\x)/-256.2});
	\draw [dash pattern=on 1pt off 1pt,domain=-3.76:9.76] plot(\x,{(-1.82--0.86*\x)/-0.5});
	\draw [domain=-3.76:9.76] plot(\x,{(-0.12-0.5*\x)/-0.86});
	\begin{scriptsize}
	\fill [color=qqqqff] (-2,1.2) circle (1.5pt);
	\draw[color=qqqqff] (-2.06,1.62) node {$A$};
	\fill [color=qqqqff] (0.54,-2.06) circle (1.5pt);
	\draw[color=qqqqff] (0.7,-1.8) node {$B$};
	\fill [color=qqqqff] (2.36,7.2) circle (1.5pt);
	\draw[color=qqqqff] (2.52,7.46) node {$C$};
	\fill [color=qqqqff] (9.04,-1.12) circle (1.5pt);
	\draw[color=qqqqff] (9.2,-0.86) node {$D$};
	\fill [color=uququq] (-0.11,3.8) circle (1.5pt);
	\draw[color=uququq] (0.06,4.4) node {$E$};
	\fill [color=uququq] (3.13,-1.77) circle (1.5pt);
	\draw[color=uququq] (3.28,-1.52) node {$F$};
	\fill [color=uququq] (1.51,1.01) circle (1.5pt);
	\draw[color=uququq] (1.6,1.44) node {$G$};
	\end{scriptsize}
	\end{tikzpicture}
	\]
	e isto finalmente conclui esta construção, pois, aplicando os lemas
	anteriores nós efetivamente cobrimos todos os casos: 
	se os segmentos se encontram, está resolvido; se não se encontram e 
	o envólucro é triangular, está resolvido; se não for triangular, é 
	quandrangular; se o ângulo formado for obtuso, redusimos para o caso
	agudo; se for agudo, acabamos de mostrar. 
	\begin{center}\it
		O dor do matemático ao simplificar,\\ 
		claro, costuma ser maior\\ 
		do que a do problema simplificado.\\$ $\\
		\(\qed\)
	\end{center}
\end{document}
