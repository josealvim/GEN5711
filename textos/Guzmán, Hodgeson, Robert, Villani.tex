\documentclass{article}

\usepackage[portuguese]{babel}
\usepackage{hyperref}
\usepackage{pgf,tikz}
\usetikzlibrary{arrows}

\definecolor{uququq}{rgb}{0.25,0.25,0.25}
\definecolor{zzttqq}{rgb}{0.6,0.2,0}
\definecolor{qqqqff}{rgb}{0,0,1}

\newcommand\ellipsis{[\ldots]}

\begin{document}
	\title{
		Resumo: \\
		\textbf{
			Difficulties in the Passage from Secundary to Tertiary 
			Education
		}\\
		de\\
		\textbf{
			Miguel de Gusmán,
			Bernard R. Hodgson,
			Aline Robert, 
			Vinicio Villani
		}
	} 
	
	\author{
		José Goudet Alvim\\
		\textnumero{}USP 9793968\\
		\texttt{jose.alvim@usp.br}
	}

	\maketitle

	O texto, publicado nos proceedings do International Congress of 
	Mathematicians, trata da aparente dificuldade associada à matemática no
	ensino superior observado em diversas partes do mundo; a discussão é 
	divida em -- essencialmente -- três partes: duas para explicar a 
	perspectiva de estudantes e professores, e uma para tratar da natureza
	das dificuldades encontradas e mitigações potenciais.
	
	Como está assim dividido, faz sentido analizar o texto também em três
	partes, referentes às correspondentes no texto.

	\section{Perspectivas de Estudantes}
	A sugestão da pesquisa informal apresentada é que uma maioria (64\%)
	dos alunos especializando-se em matemática não responderam que se 
	depararam com dificuldades na transição para o ensino superior; já no 
	grupo de professores do ensino secundário, a situação se inverte e 
	(61\%) deste afirmaram que passaram por dificuldades na transição.
	Na engenharia, a situação está mais alinhada com o curso de 
	especialização em matemática, 68\% não afirmando que passaram por 
	dificuldades na transição.
	
	Um fato interessante de se observar dos resultados é que a resposta
	neutra é sempre menor que as adjacentes, sugerindo alguma 
	heterogeneidade nos grupos. Como se houvesse “dois tipos” de estudantes
	contribuindo para à distribuição que vemos. Claro, esta análise fica 
	reservada para uma pesquisa mais séria -- estatisticamente falando.

	Os fatos apontandos da disparidade entre quão abstrata é a matemática 
	no ensino superior versus a do secundário não me parece anômala, mas  
	sugere que a comparação das dificuldades da engenharia com, digamos, a 
	dos estudantes do grupo 2, não é tão apta. Pois o fato dos estudantes
	destes grupos discordarem sobre o nível de abstração do conteúdo parece
	implicar que os conteúdos são \emph{diferentes}. Mesmo que na ementa 
	esteja disposto da mesma maneira, uma versão abstrata do mesmo material
	constiui uma \emph{diferença material}. Digo isso devido à afirmação:
	\begin{quote}\it
		In fact, a clear outcome of the data from Université Laval is 
		that the transition to university mathematics appears much
		smoother for engineering students than for preservice secondary 
		school teachers or for students of the mathematics program.
	\end{quote}
	mas se os estudantes de engenharia \emph{não acreditam} estar vendo o 
	conteúdo com um maior nível de abstração e resolvendo problemas 
	significativamente mais complexos, eles não devem estar vendo as mesmas
	coisas que os outros grupos.

	Há uma diferença aí que precisa ser explicitada: a dificuldade de um 
	tópico não tem -- necessariamente -- relação com a dificuldade sentida
	por um dado aluno. Um tópico pode ser complexo e abstrato sem que, no 
	entanto, a turma sofra com e por isso; Isto é evidente e incontroverso, 
	acredito: é para isso que existe o estudo do ensino de matemática! 

	O ponto é: os grupos 1 e 2 sentem um mesmo nível de abstração e 
	complexidade de exercícios, divergem do grupo 3 neste sentido; os 
	grupos 1 e 3 \emph{sentem} menos dificuldades que o grupo 2, sendo o 
	3 o que menos sofre com a transição. Mas dizer que o grupo 3 tem uma 
	transição para a matemática mais suave que os outros não é, na minha
	opinião, correto. É como dizer que o Grupo A cozinha melhor porque o 
	ovo frito deles ficou melhor que o croissant do grupo B; se as 
	atividades não são comparáveis, o desempenho também não é(!).

	Os outros resultados são mais interessantes, como a homogeniedade das
	respostas nas universidades Francesas e Espanholas; apesar dos 
	resultados não serem inesperados, eu acredito que as perguntas podem 
	não ter revelado tudo que poderiam: a insistência em “abstração” como
	fonte de dificuldade é paradoxal. Abstraír é magnificar o relevante 
	e minificar o irrelevante, entender coisas abstratamente é \emph{sim}
	mais fácil. Porque há menos coisas para se pensar a respeito, menos 
	propriedades extrenuósas/irrelevantes. 

	A dificuldade “vem” da abstração de duas formas: um conhecimento 
	abstrato não costuma ajudar na resolução de problemas concretos; e um
	conhecimento abstrato, para ser construído n'uma mente, exige que as 
	coisas sejam feitas na ordem apropriada, com questionamentos e 
	instigações. Teoria de Categorias, “\emph{abstract nonsense}” por 
	excelência, é praticamente impossível de se aprender sem exemplos 
	concretos. 

	O meu ponto é, a abstração é como uma perna de um corpo, a outra é o 
	conhecimento prático: \emph{exemplos}, \emph{motivação}, e teoría 
	prévia. Você planta um pé no chão e anda com o outro, planta o outro 
	e anda com o um. Geralmente é mais fácil chegar a qualquer lugar
	(exceto pelo chão) sem pular com uma perna só, e para mim é muito 
	estranho que tantas pessoas atribuam à perna esquerda a culpa da falta
	da perna direita.

	As outras críticas por parte dos alunos são menos contenciósas, e os 
	autores reconhecem problemas que eu também vejo. Como no caso do 
	comentário que não percebe que \emph{muito} do trabalho de estudantes
	é feito fora da sala; ou que o aluno preferiria ter uma apostila, 
	possivelmente sem mesmo consultar a bibliografia do curso. É 
	\emph{necessário} estudar fora da sala, horas de estudo independente 
	diariamente. 

	A citação de Zucker \cite{Zucker} é acertada na medida 
	que o aprendizado no ensino superior é largamente responsabilidade do
	aluno, e isso é aceitável porque o ensino superior é facultativo e 
	exige responsabilidade e interesse próprio do aluno, coisas que não
	são o caso no ensino básico ou médio. 

	O problema de alunos não saberem produzir provas e absorver os seus 
	conteúdos eu já toquei em textos passados. Para mim, e para muitos 
	docentes, a produção de provas é atividade central da matemática 
	superior; consequentemente, o descaso e abano de mãos com qual o tema
	é tratado -- porque professores muitas vezes fazem umas demonstrações 
	bem “mais-ou-menos” e ensinam seus alunos a fazerem por imitação -- e 
	ensinado é realmente um problema grave. 

	\section{Ponto de Vista Docente}
	Os autores relatam a insatisfação de membros do corpo docente com os 
	conhecimentos prévios de suas turmas -- coisa que aconteceu comigo, 
	pois as aulas de matemática do meu 2º e 3º ano do colégio foram 
	“\emph{extremalmente} subótimas”, isto é: péssimas --; um outro 
	problema relatado foi a falta de interesse por parte de alunos que 
	dependem da matemática para suas áreas mas não tem paixão pelo tópico
	\textit{per se}.

	Adicionalmente, deploram [sic] a falta de formalismo -- que não é 
	sempre o caso que eles próprios possuem, como amplamente aparente
	para quem sabe\footnote{
		Tem um exemplo escabroso de teoria de grafos que me forçou a 
		trancar a disciplina pois seja lá quem estava corrigindo 
		minhas listas não compreendia minhas demonstrações. 

		Outro 
		ocaso deprimente foi com um “bixo” meu que estava fazendo 
		alguma disciplina e ele provou a igualdade entre dois 
		conjuntos por extensionalidade e o monitor \emph{rejeitou} a 
		resposta porque “para provar uma igualdade você \emph{precisa} 
		provar que \(X\subseteq Y\) e \(Y\subseteq X\)”. Às vezes a 
		vontade é de rasgar os diplomas de umas pessoas.
	} -- e o conhecimento de métodos algorítmicos para resoluções de 
	problemas (onde muitas vezes, isto não é dito, os domínios dos 
	algorítmos não são conhecidos, só as regras).

	\section{Tipos de Dificuldades}
	\subsection{Epistemológicas e Cognitivas}
	Os alunos, como eu indiquei na sessão sobre suas perspectivas, tem 
	domínio e costume heterogêneo sobre as faculdades necessárias para o 
	pensamento abstrato, em uns raros há já até experiência, para outros há
	as condições necessárias para o desenvolvimento, e para outros há até 
	uma forma de atrofía devido às suas condições.

	Por esta razão, a falta de objetivos claros em aulas e expectativas mal 
	informadas por parte dos professores levam a alguma medida de dor por 
	parte da turma. Outro problema relatado, ocasionado pela falta de 
	compreensão de lógica básica por parte dos alunos, que não se sentem no 
	direito de analizar uma demonstração criticamente. Demonstrações são 
	a suspensão de autoridade, porque não há autoridade no argumento formal
	de uma prova; é curioso que os alunos tenham sido expostos à produção 
	de demonstrações na infância e ainda assim não sejam capazes determinar
	se uma demonstração está correta. Isto indica que o conhecimento de 
	produções de demonstração provavelmente não era relacional, porque 
	verificar a validade de uma demonstração é -- relativamente a produzir 
	a demonstração -- trivial.

	\subsection{Sociológicas e Culturais}
	A questão do anonimato e pequeneza de um estudante individual no ensino
	superior é interessante, porque ela não é o caso no nosso instituto 
	haja visto que as turmas são pequenas, aproximadamente o tamanho da 
	minha turma de colégio. Mas imagina-se que a relação menos pessoal do 
	aluno com o professor e possivelmente com a sua própria turma deve, de 
	alguma forma alienar -- ou pelo menos incomodar -- o estudante.

	Este problema, afirmam, é complicado pela mudança constante de colegas 
	nas turmas semestre-a-semestre. O que é um pouco estranho, porque até 
	onde eu sei, um aluno será acompanhado por seus colegas na maior parte 
	das disciplinas obrigatórias, mesmo em colegas e amigos que reprovaram
	disciplinas-chave como Cálculo I vieram a ter aulas de outras 
	disciplinas comigo. Um aspecto essencial para a formação de comunidade 
	na minha turma foi uma atitude -- agora em retrospecto, extremamente 
	importante para a minha saúde mental -- do meu professor de estatística
	no primeiro semestre, que nos colocou em grupos para a entrega de 
	exercícios; e a atividade de aprendizado necessitava a formação de 
	vínculos com colegas quase seis anos depois \emph{ainda} estão nas 
	mesmas disciplinas que eu!

	O problema da cultura de competitividade, possivelmente originada na 
	natureza excessivamente competitiva dos processos de ingresso nas 
	instituições de ensino superior, podem -- e deveriam, honestamente -- 
	causar muito desconforto e dificuldade. Não ter colegas com quem contar
	para entender um assunto ou estudar para provas é algo terrível; e 
	inclusive, anti-científico. O conhecimento se constitui em comunidade,
	o aprendizado não é mesquinho e pesquisa é feita com amigo. Novamente, 
	eu não me recordo de ter sentido que meus colegas competiam comigo, 
	coisa que eu atribuo às primeiras coisas que nossa turma ouviu na 
	recepção de ingressantes. 

	Mas ingressantes mais jovens, que estão no seu segundo ano e ainda 
	não colocaram um pé sequer dentro do IME, não tiveram esta sorte, e 
	tem \emph{muito} mais dificuldade de se relacionar com seus colegas e 
	-- portanto -- aprender e se manter engajado no curso.

	Os autores citam a “democratização do ensino” como causa de alunos 
	“fracos” ingressando nas faculdade. É uma inversão, a causa real é 
	a qualidade do ensino público em geral, e o elitismo que isto gera.
	A falta de experiência e cultura matemática é o que gera a dificuldade
	de aprendizado; as condições do ensino público podem não ser condusivas
	para o desenvolvimento de uma disciplina de estudo. E o fato de que 
	geralmente a população que recorre à educação pública é socialmente 
	vulnerável indica que estes alunos talvez não tenham nem a condição 
	material de desenvolver estas habilidades: por falta de local de 
	estudo, tempo -- melhor gasto ajundando no sustento de sua família --,
	por falta de material e estímulo, etc.

	Sobretudo, o problema citado não é nem necessariamente a fragilidade 
	do ensino público em uma sociedade desigual. Mas sim a necessidade 
	crescente de se ter um diploma de curso superior -- especialmente 
	evidente no Brasil -- para ingressar no mercado de trabalho onde eles
	são largamente desnecessários. 

	Este fato gera dois problemas, forçar população vulnerável a fazer 
	cursos com demandas de sua educação que ela não pode atender; e 
	forçar a população jovem em geral a se submeter a uma outra etapa 
	“obrigatória” de sua educação para fazer algo que tem pouco a ver 
	com o curso, como se fosse uma continuação do ensino secundário.

	E por fim, a cultura docente nas universidades é focada em pesquisa
	-- coisa que é a função histórica de universidades, não formação de 
	mão de obra, isto é largamente uma invenção do Capitalismo. Mas 
	ensino ocorre nas universidade, e consequentemente a sua falta de 
	ênfase acarreta problemas para o mesmo.

	\subsection{Didáticas e Pedagógicas}
	Da parte dos professores, uma série de falhas contribuem para as 
	dificuldades, entre elas
	\begin{enumerate}
		\item Falta de habilidades didáticas e pedagógicas;
		\item Falta de exemplos de bons professores para imitação;
		\item Negligência da metodologia do tema;
		\item Ensino pouco imaginativo e engagante;
		\item Pouca atenção dada ao projeto de curso;
		\item Avaliações inefetivas e infrequêntes sem retorno para a turma.
	\end{enumerate}

	\begin{thebibliography}{mmm}
		\bibitem [\bf Zuck] {Zucker} 
			S. Zucker, “Teaching at the university level”
			Notices of the American Ailathematical Society 43
			(1996) pp. 863-865.
	\end{thebibliography}
\end{document}
