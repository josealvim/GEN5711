\documentclass{article}

\usepackage[portuguese]{babel}
\usepackage{hyperref}

\newcommand\ellipsis{[\ldots]}

\begin{document}
	\title{
		Resumo: \\
		\textbf{
			Relational Understanding and Instrumental Understanding
		}\\
		de\\
		\textbf{Richard Skemp}\\
	} 
	
	\author{
		José Goudet Alvim\\
		\textnumero{}USP 9793968\\
		\texttt{jose.alvim@usp.br}
	}

	\maketitle

	O texto oferece dois focos distintos de discussão, o primeiro é fazer 
	evidente a existência de duas conotações por conhecimento e sobretudo 
	compreensão -- a compreensão relacional e a instrumental; o segundo 
	foco é o evidenciamento -- de um fato que eu já mencionei em resumos 
	passados -- que há também duas matemáticas, \emph{A  M}atemática (já
	evidenciando o meu viés, semelhante ao de Skemp) e a matemática 
	instrumental.

	A discussão então se torna conhecer qual são as vantagens de cada forma
	e como elas vão interagir e moldar o aprendizado de um dado aluno em 
	uma dada turma. Os casos dos quais ele trata são de desalinhamento do
	objetivo do docente e do discente; além dos problemas que surgem quando 
	o aprendizado é demasiadamente algorítmico e o excesso de um dos 
	aspectos ofusca o outro (o formal) e a boa-intuição do tema.

	Minha primeira objeção é no uso do termo compreensão instrumental; da 
	maneira dita fica aparentado que há uma compreensão. Vamos considerar 
	a Sala Chinesa. 

	“Após um esforço bilateral, o Brasil e a China compilaram um manual que
	permite a um operador brasileiro, ignorante da língua chinesa, receber 
	uma mensagem em chinês e manipulá-la simbolicamente até obter um 
	resultado final que constitui uma resposta adequada para uma pergunta 
	contida na mensagem. Pois então colocamos um zé trancado numa sala com
	um destes manuais e na porta há uma portinhola para passarem mensagens 
	(e comida, presume-se); a sala fala Chinês?”

	Não, e nem o operador. A única coisa que se pode arguír que fala Chinês
	é o próprio manual -- de alguma forma. Um aluno, portanto, munido de 
	regras e algorítmos não sabe coisa alguma. Esta ideia de compreensão 
	não me convenceu porque quem compreende deve ser capaz de aplicar 
	intenção; e quem possuí só regras não tem como aplicá-las 
	intencionalmente. Existe, sim, conhecimento instrumental: algorítmos de
	resolução, etc. mas a compreensão não pode ser instrumental.

	Uma outra objeção é no tocante ao “objetivo do aluno”, por exemplo em:
	\begin{quote}
		Pupils whose goal is to understand instrumentally, taught by a 
		teacher who wants them to understand relationally.
	\end{quote}
	o objetivo do aluno não está em pé de igualdade com o objetivo 
	pedagógico, porque não é função social do jovem educar-se; eu acredito 
	que esta interpretação é patológica e surge da ideia estrangeira onde 
	os alunos escolhem as disciplinas que fazem, e portanto se tornam 
	cidadãos incompletos. Os com facilidade na matemática não aprendem 
	história, os com facilidade em A não aprendem B e sentem-se 
	justificados em serem ignorantes por falta de afinidade com esta ou 
	aquela área. Coisa que é perdoável em um especialista, mas não em uma
	etapa de desenvolvimento tão preciosa quanto a juventude.

	O objetivo do aluno, só não digo que deve ser desconsiderado pois isso
	é um pouco extremo, mas \emph{pode} ser desconsiderado quando 
	elaborando o que e como devemos ensinar isto ou aquilo; porque nem na 
	escola eles gostariam de estar -- é um ambiente de domação de corpos e
	mentes, afinal. O ponto é: tem que ver essa medida de consideração, não
	é verdade que importa o que a turma quer \emph{desde que o objetivo 
	pedagógico seja cumprido}. É claro que ignorar os objetivos dos jovens 
	por completo dificulta o cumprimento.

	Vamos aos pontos do advogado do Diabo, onde eu serei o promotor
	\begin{enumerate}
		\item 
		\textit{Within its own context, instrumental mathematics is 
			usually easier to understand; sometimes much easier}.

		Só se for “compreender” instrumentalmente. Mas como eu indiquei
		-- eu não acredito que há compreensão em algorítmos, e eles só
		são úteis para quem os entende; veja o exemplo do erro da 
		unidade da área de um retângulo de lados dados em unidades 
		distintas.

		\item 
		\textit{the rewards are more immediate, and more apparent}

		Pode ser, mas quem elabora estas recompensas são os professores
		através da elaboração, aplicação e devolutiva de situações e 
		problemas. É um argumento falho, porque assume que a recompensa
		não pode ser mudada. Se o professor não fizer perguntas que 
		podem ser resolvidas com o algorítmo, todos vão falhar.

		\item
		\textit{because less knowledge is involved, one can often get 
			the right answer more quickly and reliably by 
			instrumental thinking than relational}
		
		Novamente, a resposta certa para a pergunta errada; a 
		velocidade raramente é virtude no pensamento. Há sim virtude
		em ser um calculista veloze, mas não é isso que \emph{eu} acho
		que deveríamos estar ensinando às pessoas. Queremos cidadãos 
		inteligentes e críticos, senhores da faculdade lógica que é 
		inerente à nossa espécie, não calculadoras -- coisa que o 
		silício já foi domado para fazer no nosso lugar.

		E mais, “because less knowledge is involved” eu adicionaria
		“thereby less knowledge is imparted”.

		\item
		\textit{relational understanding would take too long to 
			achieve}

		Quantos \emph{anos} passamos, como sociedade, ensinando coisas
		\emph{básicas} de matemática como proporcionalidade via regra
		de três e outros métodos, só para elas não serem capazes de 
		aplicar ou de lembrarem-se disso. 

		Eu discordo do tempo necessário, me parece miopia e inércia
		acreditar que o método que exige constantes revisões e aulas de
		exercícios e repetições e correções demora \emph{menos} do que 
		cultivar o conhecimento.

		Robert C. Martin -- vulgo Uncle Bob -- engenheiro de software e
		fundador do desenvolvimento de software ágil tem uma sugestão 
		sobre como atingir velocidade no desenvolvimento de software:
		\begin{quote}\center\it
			The only way to go fast, is to go well.
		\end{quote}

		Porque isto se aplica a nós? Porque é comum no desenvolvimento
		de software -- ele afirma -- fazer mal feito para fazer 
		rapidamente; gerar problemas de compreensão ou eficiência que 
		se tornam nós que devem ser desatados para que o 
		desenvolvimento continue. 

		Não convém se alongar no tema, até porque que nunca trabalhou
		em um projeto com centenas de milhares de linhas de código --
		escritas por gente que não está nem mais na organização há 
		anos -- não vai perceber o tamanho do impacto de erros do 
		passado na adição de novas partes a um programa: do impacto de 
		um conhecimento imperfeito no aprendizado de coisas novas.
		Porque programar é uma espécie de aprendizado da organização
		a qual pertence o programa; novas partes são novos aprendizados
		e o conhecimento antigo embasam o futuro.

		\item
		\textit{relational understanding of a particular topic is too 
			difficult}

		Se me provarem isto, talvez eu comece a acreditar. Eu não creio
		que estamos nem perto da capacidade cognitiva de uma criança. 
		Um bebê em uma casa bilíngue em um país com uma terceira língua
		vai acabar por aprender as três; como é o caso dos filhos de 
		um primo meu. Se uma criança é capaz de aprender e ter fluência 
		em três línguas concomitantemente além da escola, estamos 
		desperdiçando este período de capacidade extraordinária de 
		nossos jovens. Com que propósito? Qual é a vantagem?

		\item
		\textit{That a skill is needed for use in another subject 
			(e.g. science) before it can be understood relationally}

		Mas claro, e admitindo que não possamos mudar a disposição da 
		ordem das coisas eu sou obrigado a conceder. Mas a pergunta 
		fica: “e daí?” ensine agora o método para o benefício da outra
		disciplina e ensine direito depois.

		E mais, é razoável assumir que um aluno que não compreende 
		divisão e multiplicação corretamente vai entender o conceito 
		de trabalho de uma força ao longo de um deslocamento? Não, 
		claro que não; vai, no máximo, saber aplicar a fórmula. A 
		questão se torna: “se quisermos ensinar as coisas fora de 
		ordem, devemos ensiná-las fora de ordem, certo?” -- e a 
		resposta é sim.

		\item	
		\textit{he is a junior teacher in a school where all the other 
			mathematics teaching is instrumental}

		E? Ensinar da melhor forma possível é a função social dele. 
		Evidentemente não podemos entrar em guerra com todo o sistema 
		de educação -- apesar de que eu envelheço e me torno 
		exponencialmente mais belicoso -- mas como justificativa ela 
		é muito fraca. Se estamos discutindo sobre o que é ideal, o 
		mundo real não entra na discussão. Se chegarmos à conclusão
		de que X é ideal, então estamos justificados em agir para que 
		X se torne o caso. 

		Pouco importa se uma escola não tem esta filosofia de ensino,
		para quem tem consciência leve e espírito livre sabendo que 
		está fazendo o melhor que é possível.
	\end{enumerate}

	Os pontos mais interessantes são os fatores situacionais; 
	o ponto 1. é razoável: o sucesso em avaliações é objetivo instrumental
	para muitos porque ele informa o sucesso em buscas empregatíceas etc.
	Mas a avaliação é abitrária e de natureza discricionária da parte do 
	professor. Este argumento se fortalece com o ponto 3. que afirma ser 
	difícil avaliar se alguém sabe relacional ou instrumentalmente. Eu 
	concordo, realmente é o caso. Mas a distinção entre ensinar 
	instrumental ou relacionalmente é concreta e mensuráve. É difícil, sim,
	saber como se parece uma pintura coberta; mas se torna mais fácil se 
	soubermos quem as pintou: é difícil saber como aprendeu um aluno, mas 
	é mais fácil se soubermos como foi ensinado.

	O ponto 2. é extremamente válido, é o efeito de uma teoria muito rica
	mas inacessível. Não tem solução simples, mas para o que toca a 
	matemática elementar do colegial e antes, não chega a ser um problema.
	Se torna um impasse sério quando estamos estudando “abstract nonsense”, 
	também conhecida como Teoria de Categorias, etc.

	O último ponto é delicado. É claro que devemos considerar as 
	dificuldades da categoria dos professores, mas o fator multiplicativo 
	não está a favor deles: um professor ensinando matemática 
	instrumentalmente faz -- na minha opinião -- um desserviço para a sua 
	turma. Muito melhor que não ter professor algum, que parece ser a 
	alternativa num país com o tamanho do deficit de professores como o 
	nossos. Mas a falta desta mudança por um ano vai privar um ano de 
	alunos do conhecimento -- na minha opinião -- superior. E eu acho 
	difícil de justificar isso moralmente.
\end{document}
