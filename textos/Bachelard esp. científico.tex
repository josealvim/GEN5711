\documentclass{article}

\usepackage[portuguese]{babel}
\usepackage{hyperref}

\newcommand\ellipsis{[\ldots]}

\begin{document}
	\title{
		Resumo: \\
		\textbf{
			A formação do espírito científico\\
			Cap. I -- Plano da obra
		}\\
		de\\
		\textbf{Gaston Bachelard}\\
	} 
	
	\author{
		José Goudet Alvim\\
		\textnumero{}USP 9793968\\
		\texttt{jose.alvim@usp.br}
	}

	\maketitle

	\section{Preâmbulo}
	A fim de organizar a discussão e resumo dos conteúdos do capítulo,
	vamos então dispor o conteúdo da seguinte maneira: 
	\begin{enumerate}
		\item \nameref{sobrevoo} do texto;
		\item \nameref{comments} sobre o texto;
	\end{enumerate}

	\section{Sobrevoo e Resumo}
	\label	{sobrevoo}
	O capítulo inicial do livro de Bachelard carrega consigo uma 
	epistemologia da ciência; e na sua acepção, o estabelecimento do 
	conhecimento científico é bem descrito através dos diversos obstáculos 
	-- não externos, mas de natureza psicológica, cognitiva, epistemológica
	-- que se deve superar quando tratando de um fenômeno empírico.

	Estes obstáculos podem ser estudados no campo da história da ciência e 
	no campo da educação científica; e o autor distingui a função de um
	epistemólogo e de um historiador na medida que o epistemólogo se 
	permite uma posição normativa para avaliar o efeito dos obstáculos 
	epistêmicos.

	O conhecimento descrito pela sua epistemologia se dá na aniquilação de 
	um conhecimento anterior e dos obstáculos para a produção deste 
	conhecimento. Os obstáculos epistêmicos apresentados são -- aqui em 
	termos positivos --:
	\begin{enumerate}
		\item	\emph{A Suspensão da Opinião},\\
			pois a opinião não é pensante, ela é o fim do 
			pensamento antes até dele começar. E por isso é 
			hostíl ao conhecimento científico.

		\item 	\emph{O Questionamento todos os Fenômenos},\\
			pois sem o questionamento, é impossível produzir
			qualquer forma de conhecimento científico.

		\item	\emph{A Revisão do Estabelecido},\\
			que diz respeito a não permitir que o conhecimento 
			estabelecido se torne \emph{dado} como o tal. Pois 
			nada está acabado.

		\item	\emph{A Manutenção da Solidez Conceitual},\\
			pois as ideias, como qualquer edifício ou empreitada
			humana, sofre com a erosão na medida que passa o tempo, 
			e -- nas palavras precisas do autor --
			“\textit{%
				perde aos poucos \ellipsis{} sua afiada ponta 
				abstrata}” 
			devido a analogias, abusos, explicações ruins 
			\emph{etc}.
		
		\item	\emph{A Busca Dialética por Unificação},\\
			uma vez que é do interesse de qualquer cientista fazer
			casar uma e outra teoria díspar -- vejamos o esforço 
			contínuo de fazer relatividade geral e mecânica 
			quântica se comportarem bem juntos.

			Por outro lado, algumas coisas não são cientificamente 
			reconciliáveis, como cosmologia e a teologia cristã, ou
			pelo menos irreconciliáveis na medida que queremos uma 
			fusão não destrutiva das duas. 

			E também a união de duas teorias científicas 
			necessariamente suplantaria ambas.

		\item	\emph{A Desconfiança da Semelhança}, ou da aparente 
			identidade.
	\end{enumerate}

	\section{Comentários e Elaborações}
	\label	{comments}
	O texto é um pouco -- não para dizer positivista -- mas \emph{otimista}
	em acreditar que o progresso do tempo implica em uma evolução da razão
	que então permitiria a um epistemólogo tomar os testos coligidos e 
	julgá-los a partir de sua perspectiva como se houvesse o direito de 
	fazer isso sem pelo menos uma discussão da possibilidade e problemas em
	fazê-lo.

	Certamente um epistemólogo deve -- pois só pode fazer desta maneira -- 
	julgar um retrato histórico da ciência a partir de sua perspectiva 
	também história. Mas não é por isso que as normas são objetivas -- que 
	não é o que Bachelard afirma, para deixar claro, mas é algo que podia 
	ser mal-inferido. O fato é que a razão e mente humana não, como 
	espécie, estão em uma progressão inexorável (rumo a que afinal?).
	
	A missão então de um “cienciografistas” (assim como a História tem sua 
	historiografia, as Ciências tem o seu estudo sobre si) seria entender a
	psicologia de uma época, os campos múltiplos de interpretação de termos 
	e conceitos, dos entraves que com o benefício da retrospectiva podemos 
	ver que foram ou não superados.

	Na pedagogia, ele reconhece que há uma paradoxal incapacidade de alguns
	didatas de compreender que alguém não compreenda um conceito; e que a 
	repetição ou revisão é suficiente para suprir uma necessidade de 
	reformar a realidade cognitiva de um dado aluno para que o conceito em 
	si \emph{faça sentido}. É paradoxal justamente porque este processo é 
	o processo científico condensado (e sem a sua ponta experimental), e 
	um professor de ciência deveria perceber justamente isso.

	Destas afirmações então o autor passa da ciência como objeto de estudo
	para o ensino da ciência ou pelo menos sua transmissão:
	\begin{center}\it
		Logo, toda cultura científica deve começar
		\ellipsis{} por uma catarse intelectual e afetiva.
	\end{center}
	e com isso fica claro que há muito pouco que eu possa acrescentar. A 
	ciência é (ou deve ser) a emancipatória no sentido da teoria crítica,
	e o processo catártico -- isto é, de soltura e libertação, mas também 
	do expurgo do medo -- se deve à natureza iconoclasta e continuamente 
	revolucionária exigida pela prática científica.

	\subsection{Algo a ser dito sobre a Dúvida}
	Uma coisa que não é mencionada no texto é a potencial insalubridade do
	princípio de duvidar das coisas quando em descontrole, que é algo 
	sintomatizado por filósofos do século XIX e então apontado por 
	Nietzsche (a sua maneira, como lhe era de costume fazer) e outros 
	posteriores. 

	As dúvidas e questionamentos sucitados pela ciência não são, de maneira
	geral, voltados contra ele própria; são perguntas sobre a natureza de 
	fenômenos naturais. Isto é importante porque, na meu entendimento, 
	manteve as ciências produtivas e saudáveis. É, na verdade, uma 
	alternativa ao mau presságio da passagem -- talvez a mais conhecida -- 
	de Nietzsche:
	\begin{center}\it
		He who fights with monsters might take care lest he thereby 
		become a monster.\\
		And if thou gaze long into an abyss, the abyss gazes back into 
		thee.
	\end{center}

	Em “Para Além do Bem e do Mal”, os monstros ditos são morais vazias, 
	são erros conceituais, são metafísica podre e preconceitos necessários 
	à razão e à linguagem. O cavaleiro nietzscheano da transvaloração moral 
	emprega a lâmina dupla da relatividade da verdade; que desferiu um 
	corte letal contra o próprio conceito de Deus.

	A passagem afirma e explica o perigo do instrumento: e fundamentalmente 
	nada sobrevive à análise excessiva. Nem monstros nem as coisas que 
	produzem potência, as coisas boas; e deste mesmo abismo nasce o 
	Nihilismo -- filho da Vontade de Verdade.

	Na filosofia em si não convém entrar, só que meu comentário exigia este 
	\textit{addendum} para ter impácto. Mas é algo a se considerar, qual a 
	razão de nunca ter havido um Schopenhauer da matemática ou física ou 
	outra área? Acredito que sejamos poupados deste destino porque estamos 
	bem ancorados em uma realidade empírica, e não minamos nossos próprios 
	preceitos científicos com meta-questionamentos; coisa que não é o caso
	na filosofia. É um aprendizado curioso de um homem que era 
	profundamente \emph{a}científico.

	\section{Considerações Finais}

	Ao final do capítulo, o autor faz uma observação que eu considero 
	fundamental no ambiente do ensino de \emph{matemática} que é nosso 
	curso: 
	\begin{quote}\it
		A nosso ver, essa divisão é possível porque o crescimento do 
		espírito matemático é bem diferente do crescimento do espírito 
		científico em seu esforço para compreender os fenômenos 
		físicos. Com efeito, a história da matemática é 
		maravilhosamente regular. Conhece períodos de pausa. Mas não 
		conhece períodos de erro. Logo, nenhuma das teses que 
		sustentamos neste livro se refere ao conhecimento matemático. 
		Tratam apenas do conhecimento do mundo objetivo.
	\end{quote}
	embora eu desconheça a argumentação por trás da afirmação de que a 
	matemática não tenha passado por períodos de \emph{erro} ou que sua 
	história é “regular”, e não necessariamente concorde (mais disto 
	adiante) com a afirmação, estou de acordo com a conclusão que não se 
	pode analisar o conhecimento matemático como se fosse científico ou
	como se tratasse de alguma realidade minimamente \emph{real}, leia-se:
	não excessivamente platônica. 

	O conhecimento científico tem como uma de suas pernas o conhecimento 
	experimental, e busca sobretudo modelos formais para a realidade, 
	lembrando sempre o aforismo atribuído a George Box \cite{Box}:
	\begin{center}\it
		all models are wrong but some models are useful
	\end{center}
	Já o conhecimento matemático, largamente, não precisa ser revisto ou 
	ajustado devido a observações novas; mas a literatura precisa ser 
	recontextualizada, destilada, digerida e reescrita conforme o 
	conhecimento matemático e suas práticas associadas se modificam.

	No fundo, a matemática escolar tem um papel apenas coadjuvante nas 
	ciências, ela não tem o sabor revolucionário e irreverente -- de 
	criação livre. Os obstáculos, tanto pedagógicos quanto epistemológicos
	são distintos do ensino de ciências. Não só a natureza do conhecimento 
	é distinta, mas o seu caminho também; há paralelos, na medida que há 
	uma medida de experimentação na matemática -- mas não há, por exemplo, 
	incerteza experimental.

	\begin{thebibliography}{llll}
		\bibitem [\bf GBox] {Box} 
			\textit{Statistical Control: 
			By Monitoring and Feedback Adjustment}.
			George E. P. Box, 
			Alberto Luceño. 1997.
	\end{thebibliography} 
\end{document}
